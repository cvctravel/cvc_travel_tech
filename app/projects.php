<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class projects extends Model
{

    
   
    protected $fillable = [

        'item_name',
        'description',
        'price',
        'image',
        'link',
        'cat_name',
        
        
    ];  
    public function getImageAttribute($image){
        
        return asset($image);
        
    }    
}

