<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    protected $table="services";
    
    use SoftDeletes;
    
     protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
    protected $fillable = [
        'name',
        'description',
        'img',
        'created_at',
        'updated_at',
        'deleted_at'
        
        ];
        
    public function getImageAttribute($img){
        
        return asset($img);
        
    }    
}
