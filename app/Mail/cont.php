<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class cont extends Mailable
{
use Queueable, SerializesModels;
 public $name;
 public $Email;
 public $subject;
 public $massage;
 

    public function __construct($name,$Email,$subject,$massage)
    {
        //
        
        $this->name=$name;
        $this->Email=$Email;
        $this->subject=$subject;
        $this->massage= $massage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.email');
    }
}
