<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePhasesRequest;
use App\Phases;

class phasesController extends Controller
{
    public function index()
    {
        
        abort_unless(\Gate::allows('phase_access'), 403);

        $phases = Phases::all();

        return view('admin.phases.index', compact('phases'));
    }

    public function show(Phases $phase)
    {
        abort_unless(\Gate::allows('phase_show'), 403);

        return view('admin.phases.show', compact('phase'));
    }
    
    public function edit(Phases $phase)
    {
        abort_unless(\Gate::allows('phase_edit'), 403);

        return view('admin.phases.edit', compact('phase'));
    }

    public function update(UpdatePhasesRequest $request, Phases $phase)
    {
        abort_unless(\Gate::allows('phase_edit'), 403);

        $phase->update($request->all());

        return redirect()->route('admin.phases.index');
    }
}
