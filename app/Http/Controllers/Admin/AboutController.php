<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAboutRequest;
use App\About;

class AboutController extends Controller
{
    public function index()
    {
        
        abort_unless(\Gate::allows('about_access'), 403);

        $abouts = About::all();

        return view('admin.abouts.index', compact('abouts'));
    }

    public function show(About $about)
    {
        abort_unless(\Gate::allows('about_show'), 403);

        return view('admin.abouts.show', compact('about'));
    }
    
    public function edit(About $about)
    {
        abort_unless(\Gate::allows('about_edit'), 403);

        return view('admin.abouts.edit', compact('about'));
    }

    public function update(UpdateAboutRequest $request, About $about)
    {
        abort_unless(\Gate::allows('about_edit'), 403);

        $about->update($request->all());

        return redirect()->route('admin.abouts.index');
    }
}
