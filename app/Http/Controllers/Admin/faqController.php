<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFaqRequest;
use App\Http\Requests\StoreFaqRequest;
use App\Http\Requests\UpdateFaqRequest;
use App\faqs;

class faqController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('faq_access'), 403);

        $faqs = faqs::all();

        return view('admin.faqs.index', compact('faqs'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('faq_create'), 403);

        return view('admin.faqs.create');
    }


    public function store(StoreFaqRequest $request)
    {
        abort_unless(\Gate::allows('faq_create'), 403);

        $faq = faqs::create($request->all());

        return redirect()->route('admin.faqs.index');
    }

    public function show(faqs $faq)
    {
        abort_unless(\Gate::allows('faq_show'), 403);

        return view('admin.faqs.show', compact('faq'));
    }
    
    public function edit(faqs $faq)
    {
        abort_unless(\Gate::allows('faq_edit'), 403);

        return view('admin.faqs.edit', compact('faq'));
    }

    public function update(UpdateFaqRequest $request, faqs $faq)
    {
        abort_unless(\Gate::allows('faq_edit'), 403);

        $faq->update($request->all());

        return redirect()->route('admin.faqs.index');
    }

    public function destroy(faqs $faq)
    {
        abort_unless(\Gate::allows('faq_delete'), 403);

        $faq->delete();

        return back();
    }

    public function massDestroy(MassDestroyFaqRequest $request)
    {
        faqs::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
