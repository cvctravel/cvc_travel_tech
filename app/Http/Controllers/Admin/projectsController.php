<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyprojectsRequest;
use App\Http\Requests\StoreprojectsRequest;
use App\Http\Requests\UpdateProjectsRequest;
use App\projects;
class projectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = projects::all();

        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = projects::all();
        return view('admin.projects.create',compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projects = new projects;
        $projects->item_name=$request->item_name;
        $projects->demo_account=$request->demo_account;
        $projects->demo_password=$request->demo_password;
        $projects->admin_account=$request->admin_account;
        $projects->admin_password=$request->admin_password;
        $projects->admin_link=$request->admin_link;
        $projects->description=$request->description;
        $projects->link=$request->link;
        $projects->cat_name=$request->cat_name;
        $projects->price=$request->price;
       
        /*$filenamewithext=$request->file('image')->getClientOriginalName();
        $filename=pathinfo($filenamewithext,PATHINFO_FILENAME);
        $extension=$request->file('image')->getCLientOriginalExtension();
        $storename=$filename."_".time().'.'.$extension; 
  $path=$request->file('image')->storeas('public/uploads/projects',$storename);}
  
  $projects->image=$storename;*/
            $image = $request->file('image');
        $image_new_name = time().$image->getClientOriginalName();
        $image->storeas('/public',$image_new_name);
    
        
        $projects->avatar=$image_new_name;
        
         
     
        $projects->save();
        return redirect()->route('admin.projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projects=projects::find($id);
        return view('admin.projects.show', compact('projects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects=projects::find($id);
        return view('admin.projects.edit', compact('projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

            
        public function update(Request $request, $id)
        {
            $projects =projects::find($id);
        $projects->item_name=$request->item_name;
        $projects->demo_account=$request->demo_account;
        $projects->demo_password=$request->demo_password;
        $projects->admin_account=$request->admin_account;
        $projects->admin_password=$request->admin_password;
        $projects->admin_link=$request->admin_link;
        $projects->description=$request->description;
        $projects->link=$request->link;
        $projects->cat_name=$request->cat_name;
        $projects->price=$request->price;
                
        $image = $request->file('image');
        $image_new_name = time().$image->getClientOriginalName();
        $image->storeas('/public',$image_new_name);
    
        
        $projects->avatar=$image_new_name;
                    
        $projects->save();
        return redirect()->route('admin.projects.index');
    }
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects=projects::find($id);
        $projects->delete($projects->id);

        return back();
    }
}
