<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyContactusRequest;
use App\ContactUs;

class ContactUsController extends Controller
{
    public function index()
    {
        
        abort_unless(\Gate::allows('contactus_access'), 403);

        $contacts = ContactUs::all();

        return view('admin.contactus.index', compact('contacts'));
    }

    public function show(ContactUs $contactus)
    {
        
        abort_unless(\Gate::allows('contactus_show'), 403);

        return view('admin.contactus.show', compact('contactus'));
    }
    

    public function massDestroy(MassDestroyContactusRequest $request)
    {
        ContactUs::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
