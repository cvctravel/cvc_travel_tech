<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBlogRequest;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use Illuminate\Support\Facades\File;
use App\Blogs;

class BlogController extends Controller
{
   
    public function index()
    {
        abort_unless(\Gate::allows('blog_access'), 403);

        $blogs = Blogs::all();

        return view('admin.blogs.index', compact('blogs'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('blog_create'), 403);

        return view('admin.blogs.create');
    }


    public function store(StoreBlogRequest $request)
    {
        abort_unless(\Gate::allows('blog_create'), 403);
        
        $main_img = $request->file('main_img');
        $imag_new_name = time().$main_img->getClientOriginalName();
        $main_img->move(public_path() . '/uploads/blogs/',$imag_new_name);
        
        $inner_img = $request->file('inner_img');
        $image_new_name = time().$inner_img->getClientOriginalName();
        $inner_img->move(public_path() . '/uploads/blogs/',$image_new_name);
         
        $blog = Blogs::create([
               'name'=>$request->name,
               'description'=>$request->description,
               'main_img'=>$imag_new_name,
               'inner_img'=>$image_new_name,
            
            ]);


        return redirect()->route('admin.blogs.index');
    }

    public function show(Blogs $blog)
    {
        abort_unless(\Gate::allows('blog_show'), 403);

        return view('admin.blogs.show', compact('blog'));
    }
    
    public function edit(Blogs $blog)
    {
        abort_unless(\Gate::allows('blog_edit'), 403);

        return view('admin.blogs.edit', compact('blog'));
    }

    public function update(UpdateBlogRequest $request, Blogs $blog)
    {
        abort_unless(\Gate::allows('blog_edit'), 403);
        
        if($request->hasFile('main_img')){
        $main_img = $request->file('main_img');
        $imag_new_name = time().$main_img->getClientOriginalName();
        $main_img->move(public_path() . '/uploads/blogs/',$imag_new_name);
        $blog->main_img = $imag_new_name;
        }
        
        if($request->hasFile('inner_img')){
        $inner_img = $request->file('inner_img');
        $image_new_name = time().$inner_img->getClientOriginalName();
        $inner_img->move(public_path() . '/uploads/blogs/',$image_new_name);
        $blog->inner_img = $image_new_name;
        }
        
         
        $blog->update([
            $blog->name=$request->name,
            $blog->description=$request->description,
            
            ]);
        
        $blog->save();
        return redirect()->route('admin.blogs.index');
    }

    public function destroy(Blogs $blog)
    {
        abort_unless(\Gate::allows('blog_delete'), 403);

        $blog->delete();

        return back();
    }

    public function massDestroy(MassDestroyBlogRequest $request)
    {
        Blogs::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }

}
