<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySettingRequest;
use App\Http\Requests\StoreSettingRequest;
use App\Http\Requests\UpdateSettingRequest;
use App\Settings;

class settingController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('setting_access'), 403);

        $settings = Settings::all();

        return view('admin.settings.index', compact('settings'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('setting_create'), 403);

        return view('admin.settings.create');
    }


    public function store(StoreSettingRequest $request)
    {
        abort_unless(\Gate::allows('setting_create'), 403);
        
        $image = $request->file('img');
        $image_new_name = time().$image->getClientOriginalName();
        $image->move(public_path() . '/uploads/settings/',$image_new_name);
        
        $setting = Settings::create([
               'name'=>$request->name,
               'body'=>$request->body,
               'img'=>$image_new_name,
            
            ]);

        return redirect()->route('admin.settings.index');
    }

    public function show(Settings $setting)
    {
        abort_unless(\Gate::allows('setting_show'), 403);

        return view('admin.settings.show', compact('setting'));
    }
    
    public function edit(Settings $setting)
    {
        abort_unless(\Gate::allows('setting_edit'), 403);

        return view('admin.settings.edit', compact('setting'));
    }

    public function update(UpdateSettingRequest $request, Settings $setting)
    {
        abort_unless(\Gate::allows('setting_edit'), 403);
        
        if($request->hasFile('img')){
        $image = $request->file('img');
        $image_new_name = time().$image->getClientOriginalName();
        $image->move(public_path() . '/uploads/settings/',$image_new_name);
        $setting->img = $image_new_name;
        }
        
        $setting->update([
            $setting->name=$request->name,
            $setting->body=$request->body,
            
        ]);
        
        $setting->save();
        return redirect()->route('admin.settings.index');
    }

    public function destroy(Settings $setting)
    {
        abort_unless(\Gate::allows('setting_delete'), 403);

        $setting->delete();

        return back();
    }

    public function massDestroy(MassDestroySettingsRequest $request)
    {
        Settings::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }

}
