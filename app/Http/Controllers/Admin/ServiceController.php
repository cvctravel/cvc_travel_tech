<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyServicesRequest;
use App\Http\Requests\StoreServicesRequest;
use App\Http\Requests\UpdateServicesRequest;
use App\Services;

class ServiceController extends Controller
{
   
    public function index()
    {
        abort_unless(\Gate::allows('service_access'), 403);

        $services = Services::all();

        return view('admin.services.index', compact('services'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('service_create'), 403);

        return view('admin.services.create');
    }


    public function store(StoreServicesRequest $request)
    {
        abort_unless(\Gate::allows('service_create'), 403);
        
        $image = $request->file('img');
        $image_new_name = time().$image->getClientOriginalName();
        $image->move(public_path() . '/uploads/services/',$image_new_name);
        
        $service = Services::create([
               'name'=>$request->name,
               'description'=>$request->description,
               'img'=>$image_new_name,
            
            ]);

        return redirect()->route('admin.services.index');
    }

    public function show(Services $service)
    {
        abort_unless(\Gate::allows('service_show'), 403);

        return view('admin.services.show', compact('service'));
    }
    
    public function edit(Services $service)
    {
        abort_unless(\Gate::allows('service_edit'), 403);

        return view('admin.services.edit', compact('service'));
    }

    public function update(UpdateServicesRequest $request, Services $service)
    {
        abort_unless(\Gate::allows('service_edit'), 403);
        
        if($request->hasFile('img')){
        $image = $request->file('img');
        $image_new_name = time().$image->getClientOriginalName();
        $image->move(public_path() . '/uploads/services/',$image_new_name);
        $service->img = $image_new_name;
        }
        
        $service->update([
            $service->name=$request->name,
            $service->description=$request->description,
            
        ]);
        
        $service->save();
        return redirect()->route('admin.services.index');
    }

    public function destroy(Services $service)
    {
        abort_unless(\Gate::allows('service_delete'), 403);

        $service->delete();

        return back();
    }

    public function massDestroy(MassDestroyServicesRequest $request)
    {
        Services::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }

}
