<?php

namespace App\Http\Requests;

use App\Blogs;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBlogRequest extends FormRequest
{
    
    public function authorize()
    {
        return \Gate::allows('blog_edit');
    }


    public function rules()
    {
        return [
             'name' => [
                'required',
            ],
            
        
        ];
    }
}
