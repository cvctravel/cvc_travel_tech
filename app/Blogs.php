<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blogs extends Model
{
    use SoftDeletes;
    
     protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
     protected $fillable = [
        'name',
        'description',
        'main_img',
        'inner_img',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
    public function getImageAttribute($main_img){
    
        return asset($main_img);
        

        
    }
  
}
