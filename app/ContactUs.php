<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactUs extends Model
{
    protected $table = "contactuses";
    
     use SoftDeletes;
     
      protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
     protected $fillable = [
        'name',
        'email',
        'phone',
        'massage',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
