<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Phases extends Model
{
     use SoftDeletes;
     
      protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
     protected $fillable = [
        'name',
        'body',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
