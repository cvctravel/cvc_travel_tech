<?php


    Route::get('/', 'Frontend@index');
    Route::redirect('/home', '/admin');

    Auth::routes(['register' => false]);

    Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');

    Route::delete('blogs/destroy', 'BlogController@massDestroy')->name('blogs.massDestroy');

    Route::resource('blogs', 'BlogController');
    
    
    Route::delete('services/destroy', 'ServiceController@massDestroy')->name('services.massDestroy');

    Route::resource('services', 'ServiceController');
    
    
    Route::delete('projects/destroy', 'projectsController@massDestroy')->name('projects.massDestroy');

    Route::resource('projects', 'projectsController');





    Route::delete('faqs/destroy', 'faqController@massDestroy')->name('faqs.massDestroy');

    Route::resource('faqs', 'faqController');
    
    Route::delete('abouts/destroy', 'AboutController@massDestroy')->name('abouts.massDestroy');
    Route::resource('abouts', 'AboutController');
    
    Route::delete('phases/destroy', 'phasesController@massDestroy')->name('phases.massDestroy');
    Route::resource('phases', 'phasesController');
    
    Route::delete('contactus/destroy', 'ContactUsController@massDestroy')->name('contactuses.massDestroy');
    Route::resource('contactus', 'ContactUsController');
    
    Route::delete('settings/destroy', 'settingController@massDestroy')->name('settings.massDestroy');
    Route::resource('settings', 'settingController');


});

    Route::get('about', 'Frontend@about')->name('about');
    Route::get('blog', 'Frontend@blog')->name('blog');
    Route::get('blog-list/{id}', 'Frontend@blog_list');
    Route::get('contact', 'Frontend@contact');
    Route::get('faq', 'Frontend@faq');   
    Route::post('faq','Frontend@faqs')->name('fs');
    Route::get('service-details/{id}', 'Frontend@service_detail');
    Route::get('service', 'Frontend@service');

    Route::get('projects', 'Frontend@projects');

    Route::get('phases', 'phasesController@index')->name('phases');
    Route::get('setting', 'Frontend@setting')->name('phases');
    Route::post('mail', 'Frontend@cont')->name('mail');
    Route::get('mail', 'Frontend@contactReturn');
    Route::get('details', 'detailscontroller@index');

Route::post('getall', 'Frontend@getAjax');





    
    
    
    
    
    
    

