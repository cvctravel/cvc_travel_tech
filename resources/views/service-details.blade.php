@extends('layouts.min')


@section('content')
	<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1> Service </h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="/">Home</a>
								</li>
								
								<li class="breadcrumb-item active">
									 Service 
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="ls s-pt-30 s-pt-md-50 s-pb-md-10 s-py-lg-50 c-gutter-60 c-mb-30 c-mb-md-50 overflow-visible">
				<div class="divider-65 d-none d-lg-block"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-6 service-single to_animate animate" data-animation="fadeInRight">
							<h5 class="fs-20">
								{{ $services->name }}.
							</h5>
								<p>
								{!! $services->description !!}
								</p>
						</div>
						<div class="col-lg-6 to_animate fw-column animate" data-animation="fadeInLeft">
							<img src="{{asset ('Frontend/images/service/img_2.jpg')}}"  alt="img_2.jpg">
						</div>
						</div>
					</div>
					<div class="divider-10 d-none d-lg-block"></div>
				</div>
			</section>

		

				@endsection