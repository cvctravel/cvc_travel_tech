@extends('layouts.min')


@section('content')

<style>
    .page_footer.bordered-footer .footer_logo img {
    left: 463px;
    margin-top: -157px;
    position: absolute;
    width: 205px;
}
/*.ds {*/
/*    background-color: #0a162a !important;*/
/*    color: #9a9a9a !important;*/
/*}*/
</style>



			</div>
			<span class="toggle_menu_side header-slide">
				<span></span>
			</span>

			<section class="page_slider main_slider">
				<div class="flexslider" data-nav="true" data-dots="false">
					<ul class="slides">
						<li class="ds text-center slide1">
							<span class="flexslider-overlay"></span>
							<video style="width: 100%;
                            height: auto;" controls  loop autoplay muted controls id="vid">
                              <source src="{{asset('Frontend/img/Meeting.mp4')}}" type="video/mp4">
                              <source src="movie.ogg" type="video/ogg">
                            Your browser does not support the video tag.
                            </video>
							<div class="container">
								<div class="row">
									<div class="col-12 itro_slider">
										<div class="intro_layers_wrapper">
											<div class="intro_layers">
												<div class="intro_layer" data-animation="fadeIn">
													<p class="text-uppercase intro_after_featured_word">welcome to</p>
												</div>

												<div class="intro_layer" data-animation="slideRight">
													<h2 class="text-uppercase intro_featured_word">
														Travel TECH
													</h2>
												</div>
												<div class="intro_layer" data-animation="fadeIn">
													<h3 class="intro_before_featured_word">
														<span class="color-main2">Web Design</span>,
														<span class="color-main3">Marketing</span> &
														<span class="color-main4">Branding</span>
													</h3>
												</div>
												<!--<div class="intro_layer page-bottom" data-animation="expandUp">-->
												<!--	<a class="btn btn-maincolor" href="about.html">Get Started</a>-->
												<!--</div>-->
											</div>
											<!-- eof .intro_layers -->
										</div>
										<!-- eof .intro_layers_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>
						<li class="ds text-center slide2">
							<span class="flexslider-overlay"></span>
							<img src="{{asset('Frontend/images/Software.jpg')}}" alt="">
							<div class="container">
								<div class="row">
									<div class="col-12 itro_slider">
										<div class="intro_layers_wrapper">
											<div class="intro_layers">
												<div class="intro_layer" data-animation="fadeIn">
													<h3 class="text-lowercase intro_before_featured_word">
														Online
													</h3>
												</div>
												<div class="intro_layer" data-animation="fadeIn">
													<h2 class="text-uppercase intro_featured_word">
														Marketing
													</h2>
												</div>
												<div class="intro_layer" data-animation="pullDown">
													<p class="text-uppercase intro_after_featured_word">Solutions</p>
												</div>
												<!--<div class="intro_layer page-bottom" data-animation="expandUp">-->
												<!--	<a class="btn btn-maincolor" href="about.html">Get Started</a>-->
												<!--</div>-->
											</div>
											<!-- eof .intro_layers -->
										</div>
										<!-- eof .intro_layers_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>
						<!--<li class="ds text-center slide3">-->
						<!--	<img src="{{asset('Frontend/images/TRAVEL-TECH-atm-BABYLON.png')}}" alt="" style="height:900px;">-->
						<!--	<div class="container">-->
						<!--		<div class="row">-->
						<!--			<div class="col-12 itro_slider">-->
						<!--				<div class="intro_layers_wrapper">-->
						<!--					<div class="intro_layers">-->
						<!--						<div class="intro_layer" data-animation="fadeInRight">-->
						<!--							<h2 class="text-uppercase intro_featured_word light">-->
						<!--								Modern-->
						<!--							</h2>-->
						<!--						</div>-->
						<!--						<div class="intro_layer" data-animation="fadeIn">-->
						<!--							<h2 class="text-uppercase intro_featured_word bold">-->
						<!--								Marketing & design-->
						<!--							</h2>-->
						<!--						</div>-->
						<!--						<div class="intro_layer" data-animation="fadeIn">-->
						<!--							<h2 class="text-uppercase intro_featured_word">-->
						<!--								That works-->
						<!--							</h2>-->
						<!--						</div>-->

												<!--<div class="intro_layer page-bottom" data-animation="expandUp">-->
												<!--	<a class="btn btn-maincolor" href="about.html">Get Started</a>-->
												<!--	<a class="btn btn-outline-maincolor" href="#">our folio</a>-->
												<!--</div>-->
						<!--					</div>-->
											<!-- eof .intro_layers -->
						<!--				</div>-->
										<!-- eof .intro_layers_wrapper -->
						<!--			</div>-->
									<!-- eof .col-* -->
						<!--		</div>-->
								<!-- eof .row -->
						<!--	</div>-->
							<!-- eof .container -->
						<!--</li>-->

					</ul>
				</div>
				<!-- eof flexslider -->
				<div class="flexslider-bottom d-none d-xl-block">
				    <i class="fas fa-mouse"></i>
					<!--<a href="#about" class="mouse-button animated floating">-->
					<!--    <img src="{{asset('Frontend/img/mouse_icon.png')}}">-->
					<!--</a>-->
				</div>
			</section>
			<div class="divider-10 d-block d-sm-none"></div>
			<section class="s-pt-30 s-pt-lg-50 s-pt-xl-25 ls about-home" id="about">
				<div class="divider-5 d-none d-xl-block"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
							<div class="main-content text-center">
								<div class="img-wrap text-center">
									<img src="{{asset('frontend/img/vertical_line.png')}}" alt="">
									<div class="divider-35"></div>
								</div>
								<h5>
									{{ $setting[6]->name }}
								</h5>
								<p>
									{!!$setting[6]->body!!}
								</p>
								<div class="divider-30"></div>
								<div class="img-wrap text-center">
									<img src="{{asset('frontend/img/vertical_line.png')}}" alt="">
								</div>
								<!--<div>-->
								<!--	<div class="divider-40"></div>-->
								<!--	<button type="button" class="btn btn-outline-maincolor">Get Started</button>-->
								<!--	<div class="divider-40"></div>-->
								<!--</div>-->
								<div class="img-wrap text-center">
									<img src="{{asset('Frontend/img/vertical_line.png')}}" alt="">
								</div>
								<div class="divider-10 d-none d-xl-block"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="divider-10 d-block d-sm-none"></div>
			</section>

			<section class="s-pt-30 s-pb-3 service-item2 ls animate" id="services" data-animation="fadeInUp">
				<div class="container">
					<div class="row c-mb-50 c-mb-md-60">
						<div class="d-none d-lg-block divider-20"></div>
						<div class="col-12 col-md-6 col-lg-4">
							<div class="vertical-item text-center">
								<div class="item-media">
									<img src="{{ asset ('public/uploads/settings/'.$setting[0]->img) }}" alt="">
								</div>
								<div class="item-content">
									<h6>
										<a href="/service-details/{{$service[0]->id}}">{{$setting[0]->name}}</a>
									</h6>

									<p>
										{{$setting[0]->body}}
									</p>

								</div>
							</div>
						</div>
						<!-- .col-* -->
						<div class="col-12 col-md-6 col-lg-4">
							<div class="vertical-item text-center">
								<div class="item-media">
									<img src="{{ asset ('public/uploads/settings/'.$setting[1]->img) }}" alt="">
								</div>
								<div class="item-content">
									<h6>
										<a href="/service-details/{{$service[1]->id}}">{{$setting[1]->name}}</a>
									</h6>

									<p>
										{{$setting[1]->body}}
									</p>

								</div>
							</div>
						</div>
						<!-- .col-* -->
						<div class="col-12 col-md-6 col-lg-4">
							<div class="vertical-item text-center">
								<div class="item-media">
									<img src="{{ asset ('public/uploads/settings/'.$setting[2]->img) }}" alt="">
								</div>
								<div class="item-content">
									<h6>
										<a href="/service-details/{{$service[2]->id}}">{{$setting[2]->name}}</a>
									</h6>

									<p>
										{{$setting[2]->body}}
									</p>

								</div>
							</div>
						</div>
						<!-- .col-* -->
						<div class="col-12 col-md-6 col-lg-4">
							<div class="vertical-item text-center">
								<div class="item-media">
									<img src="{{ asset ('public/uploads/settings/'.$setting[3]->img) }}" alt="">
								</div>
								<div class="item-content">
									<h6>
										<a href="/service-details/{{$service[3]->id}}">{{$setting[3]->name}}</a>
									</h6>

									<p>
										{{$setting[3]->body}}
									</p>

								</div>
							</div>
						</div>
						<!-- .col-* -->
						<div class="col-12 col-md-6 col-lg-4">
							<div class="vertical-item text-center">
								<div class="item-media">
									<img src="{{ asset ('public/uploads/settings/'.$setting[4]->img) }}" alt="">
								</div>
								<div class="item-content">
									<h6>
										<a href="/service-details/{{$service[4]->id}}">{{$setting[4]->name}}</a>
									</h6>

									<p>
										{{$setting[4]->body}}
									</p>

								</div>
							</div>
						</div>
						<!-- .col-* -->
						<div class="col-12 col-md-6 col-lg-4">
							<div class="vertical-item text-center">
								<div class="item-media">
									<img src="{{ asset ('public/uploads/settings/'.$setting[5]->img) }}" alt="">
								</div>
								<div class="item-content">
									<h6>
										<a href="/service-details/{{$service[5]->id}}">{{$setting[5]->name}}</a>
									</h6>

									<p>
										{{$setting[5]->body}}
									</p>

								</div>
							</div>
						</div>
						<!-- .col-* -->
					</div>
					<div class="pink-line text-center">
						<img src="{{asset('Frontend/img/pink_line_big.png')}}" alt="">
					</div>
				</div>
			</section>


			<section class="s-pt-100 s-pt-lg-130 ds process-part skew_right s-parallax top_white_line_big overflow-visible" id="steps">
				<div class="container">
					<div class="divider-65"></div>
					<div class="row align-items-center c-mb-20 c-mb-lg-60">
						<div class="col-12 col-lg-4">
							<div class="step-left-part text-right">
								<h2 class="step-title">
									<span class="color-main">01</span>{{$phase[0]->name}}</h2>
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<div class="step-center-part text-center">
								<img src="{{asset('Frontend/images/step_img_1.jpg')}}" alt="">
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<div class="step-right-part">
								<p class="step-text">{{$phase[0]->body}}</p>
							</div>
						</div>
					</div>

					<div class="row align-items-center right c-mb-20 c-mb-lg-60">
						<div class="col-12 col-lg-4  order-lg-3">
							<div class="step-left-part">
								<h2 class="step-title color1">
									<span class="color-main2">02</span>{{$phase[1]->name}}</h2>
							</div>
						</div>
						<div class="col-12 col-lg-4 order-lg-2">
							<div class="step-center-part text-center">
								<img src="{{asset('Frontend/images/second.jpg')}}" alt="">
							</div>
						</div>
						<div class="col-12 col-lg-4 order-lg-1 text-right">
							<div class="step-right-part ">
								<p class="step-text">{{$phase[1]->body}}</p>
							</div>
						</div>
					</div>

					<div class="row align-items-center c-mb-20 c-mb-lg-60">
						<div class="col-12 col-lg-4">
							<div class="step-left-part text-right part3">
								<h2 class="step-title">
									<span class="color-main3">03</span>{{$phase[2]->name}}</h2>
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<div class="step-center-part text-center">
								<img src="{{asset('Frontend/images/third.jpg')}}" alt="">
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<div class="step-right-part">
								<p class="step-text">{{$phase[2]->body}}</p>
							</div>
						</div>
					</div>

					<div class="row align-items-center right c-mb-20 c-mb-lg-60">
						<div class="col-12 col-lg-4  order-lg-3">
							<div class="step-left-part part4">
								<h2 class="step-title color1">
									<span class="color-main4">04</span>{{$phase[3]->name}}</h2>
							</div>
						</div>
						<div class="col-12 col-lg-4 order-lg-2">
							<div class="step-center-part text-center last">
								<img src="{{asset('Frontend/images/fourth.jpg')}}" alt="">
							</div>
						</div>
						<div class="col-12 col-lg-4 order-lg-1 text-right">
							<div class="step-right-part ">
								<p class="step-text">{{$phase[3]->body}}</p>
							</div>
						</div>
					</div>
					<div class="divider-10 d-block d-sm-none"></div>
					<div class="img-wrap text-center">
						<img src="{{asset('Frontend/img/vertical_line2.png')}}" alt="">
					</div>
					<div class=" white-button text-center">
						<a class="btn white-btn" href="https://cvctravel.tech/?cat=all">Get Started</a>
					</div>
					<div class="divider-30 d-none d-xl-block"></div>
				</div>
			</section>

			<section class="s-pt-75 s-pt-xl-50 gallery-carousel main-gallery container-px-0 dis55" id="gallery">
				<div class="container-fluid">
					<div class="img-wrap text-center">
						<img src="{{asset('Frontend/img/vertical_line.png')}}" alt="">
						<div class="divider-40 d-block d-sm-none"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="row justify-content-center">
								<div class="col-md-12 col-xl-12">
								    <h3 style="    text-align: center;
    color: #e4834b;">Our portofolio</h3>
									 <div class="container my-4">




<!--Carousel Wrapper-->
<div id="multi-item-example1" class="carousel slide carousel-multi-item" data-ride="carousel">

  <!--Controls-->
  <div class="controls-top">
	<a class="btn-floating" href="#multi-item-example1" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
	<a class="btn-floating" href="#multi-item-example1" data-slide="next"><i class="fa fa-chevron-right"></i></a>
  </div>
  <!--/.Controls-->

  <!--Indicators-->
  
  <!--/.Indicators-->

  <!--Slides-->
  <div class="carousel-inner dis992" role="listbox">

	<!--First slide-->
	<div class="carousel-item active">

	  <div class="row">
	  <div class="col-md-4 col-lg-4 col-sm-6 col-xl-4">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/techlogo.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Cadence Travel Center</h6>
			  
  </div>
</div>
</div></center>

		<div class="col-md-4 col-lg-4 col-sm-6 col-xl-4">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/ltflogo.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Live Travel Fairs</h6>
			  
  </div>
</div>
</div></center>

<div class="col-md-4 col-lg-4 col-sm-6 col-xl-4">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="width:100%;max-height:100%" src="{{asset('uploads/projects/1594295213ecommerce-business.jpg')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">POS</h6>
			 
  </div>
</div>
</div></center>
<div>
	  </div>
	  </div> 
	  &nbsp
	  
	  </div>
	  
	<!--/.First slide-->

	<!--Second slide-->
	

	<div class="carousel-item">
	<div class="row">
	<div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
			   <center><div class="card" style="width: 30rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/croco.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Croconile Travel</h6>
			 
  </div>
</div>
</div></center>

<div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
			   <center><div class="card" style="width: 30rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="width:100%;max-height:100%" src="{{asset('uploads/projects/159412288183145939_2639039439646114_4791837387318224691_n.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Cadence City Guide</h6>
			  
  </div>
</div>
</div></center>
</div> 
	  &nbsp
	  
	  </div>
		
	  </div>

	</div>
	
	<!--/.Second slide-->

	<!--Third slide-->
	
	<!--/.Third slide-->

  </div>
  <!--/.Slides-->

</div>
<!--/.Carousel Wrapper-->


</div>
			</section>
			<section class="s-pt-75 s-pt-xl-50 gallery-carousel main-gallery container-px-0 dis77" id="gallery">
				<div class="container-fluid">
					<div class="img-wrap text-center">
						<img src="{{asset('Frontend/img/vertical_line.png')}}" alt="">
						<div class="divider-40 d-block d-sm-none"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="row justify-content-center">
								<div class="col-md-12 col-xl-12">
								    <h3 style="    text-align: center;
    color: #e4834b;">Our portofolio</h3>
									 <div class="container my-4">




<!--Carousel Wrapper-->
<div id="multi-item-example2" class="carousel slide carousel-multi-item" data-ride="carousel">

  <!--Controls-->
  <div class="controls-top">
	<a class="btn-floating" href="#multi-item-example2" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
	<a class="btn-floating" href="#multi-item-example2" data-slide="next"><i class="fa fa-chevron-right"></i></a>
  </div>
  <!--/.Controls-->

  <!--Indicators-->
  
  <!--/.Indicators-->

  <!--Slides-->
  <div class="carousel-inner dis992" role="listbox">

	<!--First slide-->
	<div class="carousel-item active">

	  <div class="row">
	  <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/techlogo.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Cadence Travel Center</h6>
			  
  </div>
</div>
</div></center>
</div></div>
<div class="carousel-item">
	
	<div class="row">
		<div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/ltflogo.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Live Travel Fairs</h6>
			  
  </div>
</div>
</div></center>
</div></div>
<div class="carousel-item">
	
	<div class="row">
<div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="width:100%;max-height:100%" src="{{asset('uploads/projects/1594295213ecommerce-business.jpg')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">POS</h6>
			 
  </div>
</div>
</div></center>
</div></div>
<div class="carousel-item">
	
	<div class="row">
	<div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/croco.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Croconile Travel</h6>
			 
  </div>
</div>
</div></center>
		

		
</div></div>
<div class="carousel-item">
	<div class="row">
<div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('uploads/projects/159412288183145939_2639039439646114_4791837387318224691_n.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Cadence City Guide</h6>
			  
  </div>
</div>
</div></center>

		
	  </div>

	</div>
	<!--/.Second slide-->

	<!--Third slide-->
	
	<!--/.Third slide-->

  </div>
  <!--/.Slides-->

</div>
<!--/.Carousel Wrapper-->


</div>
			</section>
				<section class="s-pt-75 s-pt-xl-50 gallery-carousel main-gallery container-px-0 dis99" id="gallery">
				<div class="container-fluid">
					<div class="img-wrap text-center">
						<img src="{{asset('Frontend/img/vertical_line.png')}}" alt="">
						<div class="divider-40 d-block d-sm-none"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="row justify-content-center">
								<div class="col-md-12 col-xl-12">
								    <h3 style="    text-align: center;
    color: #e4834b;">Our portofolio</h3>
									 <div class="container my-4">




<!--Carousel Wrapper-->
<div id="multi-item-example3" class="carousel slide carousel-multi-item" data-ride="carousel">

  <!--Controls-->
  <div class="controls-top">
	<a class="btn-floating" href="#multi-item-example3" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
	<a class="btn-floating" href="#multi-item-example3" data-slide="next"><i class="fa fa-chevron-right"></i></a>
  </div>
  <!--/.Controls-->

  <!--Indicators-->
  <ol class="carousel-indicators">
	<li data-target="#multi-item-example3" data-slide-to="0" class="active"></li>
	<li data-target="#multi-item-example3" data-slide-to="1"></li>
	<li data-target="#multi-item-example3" data-slide-to="2"></li>
	<li data-target="#multi-item-example3" data-slide-to="3"></li>
	<li data-target="#multi-item-example3" data-slide-to="4"></li>
  </ol>
  <!--/.Indicators-->

  <!--Slides-->
  <div class="carousel-inner dis992" role="listbox">

	<!--First slide-->
	<div class="carousel-item active">

	  <div class="row">
	  <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/techlogo.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Cadence Travel Center</h6>
			  
  </div>
</div>
</div></center>

		<div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/ltflogo.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Live Travel Fairs</h6>
			  
  </div>
</div>
</div></center>
</div></div>
<div class="carousel-item">
	
	<div class="row">
<div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="width:100%;max-height:100%" src="{{asset('uploads/projects/1594295213ecommerce-business.jpg')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">POS</h6>
			 
  </div>
</div>
</div></center>

	<div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="max-width:100%;max-height:100%" src="{{asset('Frontend/images/croco.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Croconile Travel</h6>
			 
  </div>
</div>
</div></center>
		

		
</div></div>
<div class="carousel-item">
	<div class="row">
<div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
			   <center><div class="card" style="width: 30rem;margin-top:10px;height:350px;position:relative;">
			  
			   
			   <div style="height:40%;">
			   <img style="width:100%;max-height:100%" src="{{asset('uploads/projects/159412288183145939_2639039439646114_4791837387318224691_n.png')}}"> 
			   </div>
  <div class="card-body">
 
  <h6 class="card-title">Cadence City Guide</h6>
			  
  </div>
</div>
</div></center>

		
	  </div>

	</div>
	<!--/.Second slide-->

	<!--Third slide-->
	
	<!--/.Third slide-->

  </div>
  <!--/.Slides-->

</div>
<!--/.Carousel Wrapper-->


</div>
			</section>
			<!--<section class="page_slider team_slider" id="team">-->
			<!--	<div class="container-fluid">-->
			<!--		<div class="row">-->
			<!--			<div class="shortcode-team-slider main-team">-->
			<!--				<h3 class="slider-title">Team</h3>-->
							<!--<div class="flexslider team-slider" data-nav="false" data-dots="true">-->
							<!--	<ul class="slides">-->
							<!--		<li class="ls">-->
							<!--			<img src="images/team/team_slide_01.jpg" alt="">-->
							<!--		</li>-->
							<!--		<li class="ls">-->
							<!--			<img src="images/team/team_slide_02.jpg" alt="">-->
							<!--		</li>-->
							<!--		<li class="ls">-->
							<!--			<img src="images/team/team_slide_03.jpg" alt="">-->
							<!--		</li>-->
							<!--		<li class="ls">-->
							<!--			<img src="images/team/team_slide_04.jpg" alt="">-->
							<!--		</li>-->
							<!--	</ul>-->
							<!--</div>-->
			<!--				 eof flexslider -->
			<!--				<div class="flexslider-controls">-->
			<!--					<ul class="flex-control-nav-1">-->
			<!--						<li class="menu_item flex-active">-->
			<!--							Gregory F. Parrino-->
			<!--							<span class="position">CEO</span>-->
			<!--							<span class="team-social-icons">-->
			<!--								<span class="social-icons">-->
			<!--									<a href="#" class="fa fa-facebook color-icon border-icon rounded-icon" title="facebook"></a>-->
			<!--									<a href="#" class="fa fa-twitter color-icon border-icon rounded-icon" title="twitter"></a>-->
			<!--									<a href="#" class="fa fa-google color-icon border-icon rounded-icon" title="google"></a>-->
			<!--								</span>-->
			<!--							</span>-->
			<!--						</li>-->
			<!--						<li class="menu_item">-->
			<!--							Letha L. Young-->
			<!--							<span class="position">Designer</span>-->
			<!--							<span class="team-social-icons">-->
			<!--								<span class="social-icons">-->
			<!--									<a href="#" class="fa fa-facebook color-icon border-icon rounded-icon" title="facebook"></a>-->
			<!--									<a href="#" class="fa fa-twitter color-icon border-icon rounded-icon" title="twitter"></a>-->
			<!--									<a href="#" class="fa fa-google color-icon border-icon rounded-icon" title="google"></a>-->
			<!--								</span>-->
			<!--							</span>-->
			<!--						</li>-->
			<!--						<li class="menu_item">-->
			<!--							Harold D. Cote-->
			<!--							<span class="position">Developer</span>-->
			<!--							<span class="team-social-icons">-->
			<!--								<span class="social-icons">-->
			<!--									<a href="#" class="fa fa-facebook color-icon border-icon rounded-icon" title="facebook"></a>-->
			<!--									<a href="#" class="fa fa-twitter color-icon border-icon rounded-icon" title="twitter"></a>-->
			<!--									<a href="#" class="fa fa-google color-icon border-icon rounded-icon" title="google"></a>-->
			<!--								</span>-->
			<!--							</span>-->
			<!--						</li>-->
			<!--						<li class="menu_item">-->
			<!--							Oren R. Odom-->
			<!--							<span class="position">Marketer</span>-->
			<!--							<span class="team-social-icons">-->
			<!--								<span class="social-icons">-->
			<!--									<a href="#" class="fa fa-facebook color-icon border-icon rounded-icon" title="facebook"></a>-->
			<!--									<a href="#" class="fa fa-twitter color-icon border-icon rounded-icon" title="twitter"></a>-->
			<!--									<a href="#" class="fa fa-google color-icon border-icon rounded-icon" title="google"></a>-->
			<!--								</span>-->
			<!--							</span>-->
			<!--						</li>-->
			<!--					</ul>-->
			<!--				</div>-->
			<!--			</div>-->
			<!--		</div>-->
			<!--	</div>-->
			
			
			<!--</section>-->

			<section class="ls ms book-item s-pb-30 s-pb-lg-25">
				<div class="corner corner-light"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<div class="text-block text-center">
								<!--<div class="btn-book-section overflow-visible">-->
								<!--	<a href="#" class="btn btn-maincolor">Get Started</a>-->
								<!--</div>-->
								<div class="img-wrap text-center">
									<img src="{{asset('Frontend/img/vertical_line.png')}}" alt="">
									<div class="divider-35"></div>
								</div>
								<h5>
									{!!$setting[7]->name!!}
								</h5>
								<p>
									{{$setting[7]->body}}
								</p>
								<div class="divider-30"></div>
								<div class="img-wrap text-center">
									<img src="{{asset('Frontend/img/vertical_line.png')}}" alt="">
								</div>
							</div>
						</div>
						<div class="divider-30"></div>
						<!--<div class="text-center img-wrap col-md-12">-->
						<!--	<div>-->
						<!--		<img src="img/vertical_line.png" alt="">-->
						<!--	</div>-->
						<!--	<div class="divider-40"></div>-->
						<!--	<a href="#" class="btn btn-outline-maincolor">Get Started</a>-->
						<!--	<div class="divider-40"></div>-->
						<!--	<div>-->
						<!--		<img src="img/vertical_line.png" alt="">-->
						<!--	</div>-->
						<!--</div>-->
					</div>
					<!--<div class="divider-10"></div>-->
				</div>
			</section>
			<section class="ls ms blog-post-carousel">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<div class="owl-carousel" data-responsive-lg="3" data-responsive-md="3" data-responsive-sm="2" data-responsive-xs="1" data-nav="false" data-dots="false" data-loop="true" data-margin="4">
								
								<!-- #post-## -->
								<article class="box text-center">
									<div class="item-content ">
										
										<h6>
											<a href="#">{{$setting[15]->name}}</a>
										</h6>
										<div class="post-author">
											<i class="fa fa-bed fa-2x" aria-hidden="true"></i>
											<p>
												{{$setting[15]->body}} 
											</p>
										</div>
									</div>
									<!-- .item-content -->
								</article>
								<article class="box text-center">
									<div class="item-content">
										
										<h6>
											<a href="#">{{$setting[14]->name}}</a>
										</h6>
										<div class="post-author">
										<i class="fa fa-thumb-tack fa-2x" aria-hidden="true"></i>

											<p style="font-size:14px">
												{{$setting[14]->body}}
											</p>
										</div>
									</div>
									<!-- .item-content -->
								</article>
								<article class="box text-center">
									<div class="item-content ">
										
										<h6>
											<a href="#">{{$setting[16]->name}}</a>
										</h6>
										<div class="post-author">
											<i class="fa fa-plane fa-2x" aria-hidden="true"></i>

											<p>
											 {{$setting[16]->body}} 
											</p>
										</div>
									</div>
									<!-- .item-content -->
								</article>
								<!-- #post-## -->
							
								<!-- #post-## -->
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="s-pt-130 s-pb-15 s-pb-md-50 s-pt-xl-100 s-pb-lg-30 overflow-visible s-parallax testimonials-sliders main-testimonials ds" id="testimonials">
				<div class="corner ls ms"></div>
				<div class="container">
					<div class="row c-mt-30 c-mt-md-0">
						<div class="divider-20"></div>
						<!--<div class="text-center img-wrap line col-md-12">-->
						<!--	<img src="img/vertical_line2.png" alt="">-->
						<!--</div>-->
						<!--<div class="divider-40 d-none d-md-block"></div>-->
						<!--<div class="col-md-12">-->
						<!--	<div class="owl-carousel" data-autoplay="false" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-nav="false" data-dots="true" id="quote">-->
						<!--		<div class="quote-item">-->
						<!--			<div class="quote-image">-->
						<!--				<img src="images/team/testimonials_02.jpg" alt="">-->
						<!--			</div>-->
						<!--			<p class="small-text author-job">-->
						<!--				Eye Insurance-->
						<!--			</p>-->
						<!--			<h5>-->
						<!--				<a href="#">George M. Baty</a>-->
						<!--			</h5>-->
						<!--			<p>-->
						<!--				<em class="big">-->
						<!--					I needed more leads for my services. Pay-per-click, banners of maybe even broschures. They made an analysis of my existing site. It occurred that my webdite is banned with Google, and I never knew about that!-->
						<!--				</em>-->
						<!--			</p>-->
						<!--		</div>-->
						<!--		<div class="quote-item">-->
						<!--			<div class="quote-image">-->
						<!--				<img src="images/team/testimonials_01.jpg" alt="">-->
						<!--			</div>-->
						<!--			<p class="small-text author-job">-->
						<!--				Moving co-->
						<!--			</p>-->
						<!--			<h5>-->
						<!--				<a href="#">Jeffrey P. McAllister</a>-->
						<!--			</h5>-->
						<!--			<p>-->
						<!--				<em class="big">-->
						<!--					I highly recommend this company for all and any of your design needs. I am very happy with the new redesigned and restructured website they built for my moving company!-->
						<!--				</em>-->
						<!--			</p>-->
						<!--		</div>-->
						<!--		<div class="quote-item">-->
						<!--			<div class="quote-image">-->
						<!--				<img src="images/team/testimonials_03.jpg" alt="">-->
						<!--			</div>-->
						<!--			<p class="small-text author-job">-->
						<!--				Paradox Inc-->
						<!--			</p>-->
						<!--			<h5>-->
						<!--				<a href="#">Josephine B. Anderson</a>-->
						<!--			</h5>-->
						<!--			<p>-->
						<!--				<em class="big">-->
						<!--					This guys are awesome! It is hard to find a web design company who can actually listen and understand what you need. I’m 100% satisfied with this guys. My website is exactly what I needed and even more...-->
						<!--				</em>-->
						<!--			</p>-->

						<!--		</div>-->
						<!--	</div>-->
						<!--	 .testimonials-slider -->
						<!--</div>-->
						<!--<div class="divider-55 d-none d-md-block"></div>-->
						<!--<div class="text-center img-wrap col-md-12">-->
						<!--	<img src="img/vertical_line2.png" alt="">-->
						<!--</div>-->
						<div class="divider-10 d-none d-md-block"></div>
					</div>
				</div>
				<!--<div class="testimonials-btn text-center">-->
				<!--	<a href="#quote" class="btn-maincolor">-->
				<!--		<i class="fa fa-angle-up"></i>-->
				<!--	</a>-->
				<!--</div>-->
				<div class="corner corner-light"></div>
			</section>

			<!--<section class="s-pt-130 s-pt-md-50 ls text-section">-->
			<!--	<div class="divider-30"></div>-->
			<!--	<div class="container">-->
			<!--		<div class="row">-->
			<!--			<div class="text-center col-md-12 justify-content-center text-block">-->
			<!--				<img src="img/vertical_line.png" alt="">-->
			<!--				<div class="divider-35"></div>-->
			<!--				<div class="content">-->
			<!--					<h1>-->
			<!--						Lets Get Started-->
			<!--						<br> Your Project-->
			<!--					</h1>-->
			<!--					<p>-->
			<!--						We’ll help to achieve your goals and to grow business-->
			<!--					</p>-->
			<!--					<div class="divider-30"></div>-->
			<!--				</div>-->
			<!--				<img src="img/vertical_line.png" alt="">-->
			<!--				<div>-->
			<!--					<div class="divider-40"></div>-->
			<!--					<a href="#" class="btn btn-outline-maincolor">Let’s Talk!</a>-->
			<!--					<div class="divider-30"></div>-->
			<!--				</div>-->
			<!--				<div class="img-wrap overflow-visible">-->
			<!--					<img src="img/vertical_line.png" alt="">-->
			<!--					<div class="divider-5 d-none d-xl-block"></div>-->
			<!--				</div>-->
			<!--			</div>-->

			<!--		</div>-->
			<!--	</div>-->
			<!--</section>-->


			<br>
			<br>
			<section id="work-process" style="width:100%;background-color:#303E51;no-repeat;background-size:cover;" style="@media only screen and (max-width: 375px) {
				[class*="col-6] {
				  width: 100%;
				  margin-bottom:10px;
				}
			  }>
				<div class="container" >
				<div class="section-header">
				<h2 class="color" style="text-align:center;margin-top:30px;margin-bottom:-50px">Our Process</h2>
				<br>
				<div style="height:100px;width:100px;backgroud-color:white;"></div>
			</div>
				<div class="row text-center">
						<div class="col-md-2 col-md-2 col-xs-6 col-xs-6">
				<div class="wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 0ms; animation-name: fadeInUp;">
				<div class="icon-circle">
				
				<div style="
					display: inline-block;
					width: 80px;
					height: 80px;
					line-height: 80px;
					margin-buttom:10px;
					border: 2px solid #45aed6;
					border-radius: 100px;
				position: relative;" >
				<span style="    border-style: solid;
				border-width: 2px;
				border-color: #45aed6;
				border-radius: 50%;
				background-color: #fff;
				position: absolute;
				width: 28px;
				height: 24px;
				line-height: 20px;
				top: -12px;
				color: #64686d;">1</span>
				<i class="fa fa-coffee fa-2x color" ></i>
				</div>
				</div>
				<h6 class="color" style="margin-bottom:10px;">MEET</h6>
				</div> 
				</div>
				<div class="col-md-2 col-md-2 col-xs-6 col-xs-6">
				<div class="wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="100ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 100ms; animation-name: fadeInUp;">
				<div class="icon-circle">
				
				<div style="
					display: inline-block;
					width: 80px;
					height: 80px;
					margin-buttom:10px;
					line-height: 80px;
					border: 2px solid #45aed6;
					border-radius: 100px;
				position: relative;" >
				<span style=" border-style: solid;
				border-width: 2px;
				border-color: #45aed6;
				border-radius: 50%;
				background-color: #fff;
				position: absolute;
				width: 28px;
				height: 24px;
				line-height: 20px;
				top: -12px;
				color: #64686d;">2</span>
				<i class="fa fa-bullhorn fa-2x color"></i>
			</div>
				</div>
				<h6 class="color" style="margin-bottom:10px;">PLAN</h6>
				</div>
				</div>
				<div class="col-md-2 col-md-2 col-xs-6 col-xs-6">
				<div class="wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="200ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 200ms; animation-name: fadeInUp;">
				<div class="icon-circle">
				
				<div style="
					display: inline-block;
					width: 80px;
					margin-buttom:10px;
					height: 80px;
					line-height: 80px;
					border: 2px solid #45aed6;
					border-radius: 100px;
				position: relative;" >
				<span style=" border-style: solid;
				border-width: 2px;
				border-color: #45aed6;
				border-radius: 50%;
				background-color: #fff;
				position: absolute;
				width: 28px;
				height: 24px;
				line-height: 20px;
				top: -12px;
				color: #64686d;">3</span>
				<i class="fa fa-image fa-2x color"></i>
				
				</div>
				</div>
				<h6 class="color" style="margin-bottom:10px;">DESIGN</h6>
				</div>
				</div>
				<div class="col-md-2 col-md-2 col-xs-6 col-xs-6">
				<div class="wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 300ms; animation-name: fadeInUp;">
				<div class="icon-circle">
				
				<div style="
					display: inline-block;
					width: 80px;
					margin-buttom:10px;
					height: 80px;
					line-height: 80px;
					border: 2px solid #45aed6;
					border-radius: 100px;
				position: relative;" >
				<span style=" border-style: solid;
				border-width: 2px;
				border-color: #45aed6;
				border-radius: 50%;
				background-color: #fff;
				position: absolute;
				width: 28px;
				height: 24px;
				line-height: 20px;
				top: -12px;
				color: #64686d;">4</span>
				<i class="fa fa-heart fa-2x color"></i>
			</div>
				</div>
				<h6 class="color" style="margin-bottom:10px;">DEVELOP</h6>
				</div>
				</div>
				<div class="col-md-2 col-md-2 col-xs-6 col-xs-6">
				<div class="wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 400ms; animation-name: fadeInUp;">
				<div class="icon-circle">
				<div style="
					display: inline-block;
					width: 80px;
					margin-buttom:10px;
					height: 80px;
					line-height: 80px;
					border: 2px solid #45aed6;
					border-radius: 100px;
				position: relative;" >
				<span style=" border-style: solid;
				border-width: 2px;
				border-color: #45aed6;
				border-radius: 50%;
				background-color: #fff;
				position: absolute;
				width: 28px;
				height: 24px;
				line-height: 20px;
				top: -12px;
				color: #64686d;">5</span>
				<i class="fa fa-shopping-cart fa-2x color"></i>
			</div>
				</div>
				<h6 class="color" style="margin-bottom:10px;">TESTING</h6>
				</div>
				</div>
				<div class="col-md-2 col-md-2 col-xs-6 col-xs-6">
				<div class="wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 500ms; animation-name: fadeInUp;">
				<div class="icon-circle">
				<div style="
					display: inline-block;
					width: 80px;
					margin-buttom:10px;
					height: 80px;
					line-height: 80px;
					border: 2px solid #45aed6;
					border-radius: 100px;
				position: relative;" >
				<span style=" border-style: solid;
				border-width: 2px;
				border-color: #45aed6;
				border-radius: 50%;
				background-color: #fff;
				position: absolute;
				width: 28px;
				height: 24px;
				line-height: 20px;
				top: -12px;
				color: #64686d;">6</span>
				<i class="fa fa-space-shuttle fa-2x color"></i>
				</div>
				</div>
				<h6 class="color" style="margin-bottom:10px;">LAUNCH</h6>
				</div>
				</div>
				</div>
				</div>
			
				</section>
			<br>
			<br>
			<br>
			<br>

			<section class="s-pt-50 s-pb-100 s-pt-lg-30 s-pb-lg-75 ls ms teaser-contact-icon main-icon s-parallax" id="contact">
				<div class="corner corner-inverse"></div>
				<div class="text-center img-wrap col-md-12">
					<!--<img src="img/dark_line_short.png" alt="">-->
				</div>
				<div class="container">
					<div class="divider-10 d-none d-xl-block"></div>
					<!--<div class="row c-mb-50 c-mb-lg-0">-->
					<!--	<div class="col-lg-4 text-center">-->
					<!--		<div class="border-icon">-->
					<!--			<div class="teaser-icon">-->
									<!--<img src="{{ asset ('public/uploads/settings/'.$setting[8]->img) }}" alt="">-->
					<!--				<i class="fa fa-mobile fa-2x" aria-hidden="true" style="color:#fff"></i>-->
					<!--			</div>-->
					<!--		</div>-->
					<!--		<h6>-->
					<!--		  {{$setting[8]->name}}-->
					<!--		</h6>-->
					<!--		<p>-->
					<!--		  {!! $setting[8]->body !!}-->
							  
					<!--		</p>-->
					<!--	</div>-->
					<!--	<div class="col-lg-4 text-center">-->
					<!--		<div class="border-icon">-->
					<!--			<div class="teaser-icon">-->
									<!--<img src="{{ asset ('public/uploads/settings/'.$setting[9]->img) }}" alt="" width="77px;">-->
					<!--				<i class="fa fa-pencil fa-2x" aria-hidden="true"  style="color:#fff"></i>-->
					<!--			</div>-->
					<!--		</div>-->
					<!--		<h6>-->
					<!--			{{$setting[9]->name}}-->
					<!--		</h6>-->
					<!--		<p>-->
					<!--			{!!$setting[9]->body!!}-->
							
					<!--		</p>-->
					<!--	</div>-->
					<!--	<div class="col-lg-4 text-center">-->
					<!--		<div class="border-icon">-->
					<!--			<div class="teaser-icon">-->
									<!--<img src="{{ asset ('public/uploads/settings/'.$setting[10]->img) }}" alt="">-->
					<!--				<i class="fa fa-location-arrow fa-2x" aria-hidden="true" style="color:#fff"></i>-->
					<!--			</div>-->
					<!--		</div>-->
					<!--		<h6>-->
					<!--			{{$setting[10]->name}}-->
					<!--		</h6>-->
					<!--		<p>-->
					<!--		{!!$setting[10]->body!!} ,-->
							
					<!--		</p>-->
					<!--	</div>-->
					<!--</div>-->



					<div class="row c-mt-50 c-mt-lg-0">
						<div class="col-lg-4 text-center call-icon">
							<div class="border-icon">
								<div class="teaser-icon">
										<i class="fa fa-mobile fa-2x" aria-hidden="true" style="color:#fff"></i>
								</div>
							</div>	
							<div class="icon-content">
							<h6>
							    {{$setting[8]->name}}
							</h6>
						     <p>
						         {!! $setting[8]->body !!}
							</p>
							</div>
						</div>
						<div class="col-lg-4 text-center write-icon">
							<div class="divider-30 d-none d-xl-block"></div>
							<div class="border-icon">
								<div class="teaser-icon">
								    
									<i class="fa fa-pencil fa-2x" aria-hidden="true"  style="color:#fff"></i>
								</div>
							</div>
							<div class="icon-content">
								<h6>
								{{$setting[9]->name}}
								</h6>
								<p>
								{!!$setting[9]->body!!}
								</p>
							</div>
						</div>
						<div class="col-lg-4 text-center visit-icon">
							<div class="border-icon">
								<div class="teaser-icon">
									<i class="fa fa-location-arrow fa-2x" aria-hidden="true" style="color:#fff"></i>
								</div>
							</div>
							<div class="icon-content">
								<h6>
									{{$setting[10]->name}}
								</h6>
								<p>
								    	{!!$setting[10]->body!!}
								</p>
							</div>
						</div>
					</div>
					<div class="divider-30 d-none d-lg-block"></div>
					<div class="text-center img-wrap col-md-12 layout-2">
						<img src="{{asset('Frontend/img/dark_line_short.png')}}" alt="">
					</div>
				</div>
				<div class="divider-10"></div>
			</section>
			<style>.color
			{
				color: white;
			}
			</style>

<script>
	document.getElementById('vid').play();
</script>
<style>@media screen and (max-width: 992px) {.dis55{display:none} } </style>
<style>@media screen and (min-width: 576px) {.dis77{display:none} } </style>
<style>@media screen  and (min-width: 992px){.dis99{display:none} } </style>
<style>@media screen  and (max-width: 576px){.dis99{display:none} } </style>
@endsection
