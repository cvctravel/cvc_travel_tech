@extends('layouts.min')


@section('content')

			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>Services </h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
								<a href="index-2.html">Home</a>
								</li>
								
								<li class="breadcrumb-item active">
									Services 
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="ls s-pt-50 s-pb-100 s-pt-md-75 s-pt-lg-50 s-pb-lg-130 c-mb-30 service-item1">
				<div class="d-none d-lg-block divider-65"></div>
				<div class="container">
					<div class="row">
					    @foreach($services as $kay=>$service)
						<div class="col-md-6 col-sm-12">
							<div class="icon-box text-center hero-bg">
								<div class="service-icon">
								    
									<img src="{{ asset ('public/uploads/services/'.$service->img) }}" alt="{{$service->img}}" style="width: 64px;">
								</div>
								<h6>
									<a href="/service-details/{{$service->id}}">{{$service->name}}</a>
								</h6>
								<p>
									{{ substr(strip_tags($service->description),0,50)}}
								</p>
								<a href="/service-details/{{$service->id}}" class="btn-link">Read more</a>
								
							</div>
						</div>
						<!-- .col-* -->
                        @endforeach

				</div>
				<div class="d-none d-lg-block divider-45"></div>
			</section>

	@endsection