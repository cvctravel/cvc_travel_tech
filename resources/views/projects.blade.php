@extends('layouts.min')


@section('content')
<?php
$cat=$_GET['cat'];
use App\projects;
if($cat=='all'){$projects=projects::all();}
else{
$projects= DB::table('projects')->where('cat_name',$cat)->get();
}


?>
			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<center><h1 class="marg">Projects </h1></center>
							<br>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
								<a href="projects?cat=all">all</a>
								</li>
								
								<li class="breadcrumb-item">
								<a href="projects?cat=ecommerce">ecommerce</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=hotels_and_cruises">hotels and cruises</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=tracking">tracking</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=design">design</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=accounting">accounting</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=marketing">marketing</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=mobile_applications">mobile applications</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=education">education</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=b2b-travel">b2b-travel</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=b2c-travel">b2c-travel</a>
								</li>
								<li class="breadcrumb-item">
								<a href="projects?cat=travel-expo">travel-expo</a>
								</li>
								
								<li class="breadcrumb-item">
								<a href="projects?cat=hospital_and_medical">hospital and medical</a>
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
							
						</div>
					</div>
				</div>
			</section>


			<section>
			   

			  
<div class="container">
<div class="card-group">

			   @foreach($projects as $project)
			   <div class="col-md-6 col-lg-4 col-sm-12 col-xl-4">
			   <center><div class="card" style="width: 18rem;margin-top:10px;height:350px;position:relative;">
			   <?php
                            $image=$project->avatar;
                            ?>
                            

							
			   
			   <div style="height:40%;">
			   <img style="width:100%;max-height:100%" src="{{ asset ('storage/'.$image) }}"> 
			   </div>
  <div class="card-body">
 
    <h6 style="font-size:18px" class="card-title">{{$project->item_name}}</h6>
	<p class="cvc123">by cvc travel tech</p>
	<h6 class="card-text">${{$project->price}}</h6>
    <a style="padding-left:30px;padding-right:30px;padding-top:10px;padding-bottom:10px;margin-left:150px;position:absolute;top:85%;left:9%;" href="details?id={{$project->id}}" class="btn-primary">details</a>
  </div>
</div>
</div></center>

			   @endforeach
			   </div>	 
			   </div>  
			  
			
			</section>


			<style>section{background-color:#dfe6e9;z-index:0;margin-bottom:-30px;padding:50px;margin-top:-50px;}.cvc123{color:lightgrey;font-size:12px}@media (max-width:321px){.card-group{margin-left:-60px;}}@media (min-width:323px){.card-group{margin-left:-30px}}</style>
	@endsection

	