@extends('layouts.min')
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

@section('content')

			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>Contact </h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index-2.html">Home</a>
								</li>
								
								<li class="breadcrumb-item active">
									Contact 
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="ls s-pt-30 s-pb-100 s-pb-md-130 s-py-lg-100 contact2">
				<div class="divider-15 d-none d-xl-block"></div>
				<div class="container">
					<div class="row c-mb-30 c-mb-md-50">
						<div class="col-md-4 text-center">
							<div class="border-icon">
								<div class="teaser-icon">
									<img src="{{asset('Frontend/images/icon1.png')}}" alt="">
								</div>
							</div>
							<h6>
								Call Us
							</h6>
							<p class="teaser-content">
							
								
								<strong>Support:</strong>+20 1221232504
							</p>
						</div>
						<div class="col-md-4 text-center">
							<div class="border-icon">
								<div class="teaser-icon">
									<img src="{{asset('Frontend/images/icon3.png')}}" alt="">
								</div>
							</div>
							<h6>
								Write Us
							</h6>
							<p class="teaser-content">
								info@cvctravel.tech
								
							</p>
						</div>
						<div class="col-md-4 text-center">
							<div class="border-icon">
								<div class="teaser-icon">
									<img src="{{asset('Frontend/images/icon2.png')}}" alt="">
								</div>
							</div>
							<h6>
								Visit Us
							</h6>
							<p class="teaser-content">
							11 Aladeeb  Ali adham , Sheraton, Cairo, Egypt
							
							</p>
						</div>
					</div>
					@include('errors.flash')
					<div class="divider-60 d-none d-xl-block"></div>
					<div class="row">
						<div class="col-lg-12 ">
							<form  method="POST"  action="{{route('mail')}}">
                            {{ csrf_field()  }}
								<div class="row c-gutter-20">

									<div class="col-12 col-md-6">
										<div class="form-group has-placeholder">
											<label for="name">Full Name
												<span class="required">*</span>
											</label>
											
											<input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control text-left" placeholder="Full Name">
										</div>
										<div class="form-group has-placeholder ">
											<label for="email">Email address
												<span class="required">*</span>
											</label>
											<input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control text-left" placeholder="Email Address">
										</div>
										<div class="form-group has-placeholder">
											<label for="subject">Subject
												<span class="required">*</span>
											</label>
											<input type="text" aria-required="true" size="30" value="" name="phone" id="phone" class="form-control text-left" placeholder="Phone Number">
										</div>
									</div>
									<div class="col-12 col-md-6">

										<div class="form-group has-placeholder">
											<label for="message">Message</label>
											<textarea aria-required="true" rows="6" cols="45" name="message" id="message" class="form-control text-left" placeholder="Your Message"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group text-center">
											<button type="submit" id="contact_form_submit" name="contact_submit" class="btn btn-maincolor">Send Message</button>
										</div>
									</div>

								</div>
							</form>

						</div>
						<!--.col-* -->

						<div class="divider-80 d-none d-xl-block"></div>

					</div>
				</div>
			</section>
	
	@endsection
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> 
<script>
	@if(Session::has('message'))
		var type="{{Session::get('alert-type','info')}}"
switch(type){
	        case 'success':
	            toastr.success("{{ Session::get('message') }}");
	            break;
         	 case 'error':
		        toastr.error("{{ Session::get('message') }}");
		        break;
                    }
	@endif
</script>

