<div class="sidebar">
    <nav class="sidebar-nav ps ps--active-y">

        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            
            
            
            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-users nav-icon">

                    </i>
                    {{ trans('global.userManagement.title') }}
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                            <i class="fas fa-unlock-alt nav-icon">

                            </i>
                            {{ trans('global.permission.title') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                            <i class="fas fa-briefcase nav-icon">

                            </i>
                            {{ trans('global.role.title') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                            <i class="fas fa-user nav-icon">

                            </i>
                            {{ trans('global.user.title') }}
                        </a>
                    </li>
                </ul>
            </li>
            
            
            <!--<li class="nav-item">-->
            <!--    <a href="{{ route("admin.products.index") }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">-->
            <!--        <i class="fas fa-cogs nav-icon">-->

            <!--        </i>-->
            <!--        {{ trans('global.product.title') }}-->
            <!--    </a>-->
            <!--</li>-->




            <li class="nav-item">
                <a href="{{ route("admin.blogs.index") }}" class="nav-link {{ request()->is('admin/blogs') || request()->is('admin/blogs/*') ? 'active' : '' }}">
                    <i class="fas fa-calendar-alt nav-icon">

                    </i>
                    {{ trans('global.blog.title') }}
                </a>
            </li>



   <li class="nav-item">
                <a href="{{ route("admin.services.index") }}" class="nav-link {{ request()->is('admin/service') || request()->is('admin/service/*') ? 'active' : '' }}">
                    <i class="fas fa-hand-holding-heart nav-icon">

                    </i>
                    {{ trans('global.service.title') }}
                </a>
            </li>
      
      
      
      
      
              <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-chart-bar nav-icon">

                    </i>
                    About & Phases
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("admin.abouts.index") }}" class="nav-link {{ request()->is('admin/abouts') || request()->is('admin/abouts/*') ? 'active' : '' }}">
                            <i class="fas fa-book-open nav-icon">

                            </i>
                    {{ trans('global.about.title') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("admin.phases.index") }}" class="nav-link {{ request()->is('admin/phases') || request()->is('admin/phases/*') ? 'active' : '' }}">
                            <i class="fas fa-cogs nav-icon">

                            </i>
                         {{ trans('global.phases.title') }}
                        </a>
                    </li>
                  
                </ul>
            </li>
            
        <li class="nav-item">
                <a href="{{ route("admin.contactus.index") }}" class="nav-link {{ request()->is('admin/contactus') || request()->is('admin/contactus/*') ? 'active' : '' }}">
                    <i class="fas fa-comments nav-icon">

                    </i>
                    {{ trans('global.Connectus.title') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route("admin.projects.index") }}" class="nav-link {{ request()->is('admin/projects') || request()->is('admin/projects/*') ? 'active' : '' }}">
                <i style="color:grey;" class="fas fa-project-diagram"></i>

                    </i>
                    projects
                </a>
            </li>
            
            
              <li class="nav-item">
                <a href="{{ route("admin.faqs.index") }}" class="nav-link {{ request()->is('admin/faq') || request()->is('admin/fak/*') ? 'active' : '' }}">
                    <i class="fas fa-grin-stars nav-icon">

                    </i>
                    {{ trans('global.faq.title') }}
                </a>
            </li>
                        
       <li class="nav-item">
                <a href="{{ route("admin.settings.index") }}" class="nav-link {{ request()->is('admin/settings') || request()->is('admin/settings/*') ? 'active' : '' }}">
                    <i class="fas fa-sliders-h nav-icon">

                    </i>
                    {{ trans('global.Settings.title') }}
                </a>
            </li>
            
            
        
        

            
            



            <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>
        </ul>

        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>