@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.Settings.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.Settings.fields.name') }}
                    </th>
                    <td>
                        {{ $setting->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.Settings.fields.body') }}
                    </th>
                    <td>
                        {!! $setting->body !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.Settings.fields.img') }}
                    </th>
                    <td>
                         {{ $setting->img }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection