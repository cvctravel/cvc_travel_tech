@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.Settings.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.settings.update", [$setting->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('global.Settings.fields.name') }}*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($setting) ? $setting->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.Settings.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                <label for="body">{{ trans('global.Settings.fields.body') }}</label>
                <textarea id="body" name="body" class="form-control ">{{ old('body', isset($setting) ? $setting->body : '') }}</textarea>
                @if($errors->has('body'))
                    <em class="invalid-feedback">
                        {{ $errors->first('body') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.Settings.fields.body_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('img') ? 'has-error' : '' }}">
                <label for="img">{{ trans('global.Settings.fields.img') }}</label>
                <input type="file" id="img" name="img" class="form-control" value="{{ old('img', isset($setting) ? $setting->img : '') }}">
                @if($errors->has('img'))
                    <em class="invalid-feedback">
                        {{ $errors->first('img') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.Settings.fields.img_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection