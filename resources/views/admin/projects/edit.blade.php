@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        edit projects
    </div>

    <div class="card-body">
        <form action="{{ route("admin.projects.update", [$projects->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('item_name') ? 'has-error' : '' }}">
                <label for="item_name">project name*</label>
                <input type="text" id="item_name" name="item_name" class="form-control" value="{{ old('item_name', isset($projects) ? $projects->item_name : '') }}">
                @if($errors->has('item_name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('item_name') }}
                    </em>
                @endif
                <p class="helper-block">
          
                </p>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">description</label>
                <textarea id="description" name="description" class="form-control ">{{ old('description', isset($projects) ? $projects->description : '') }}</textarea>
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                 
                </p>
            </div>
            <div class="form-group {{ $errors->has('demo_account') ? 'has-error' : '' }}">
                <label for="demo_account">demo_account</label>
                <input type="text" id="demo_account" name="demo_account" class="form-control" value="{{ old('demo_account', isset($projects) ? $projects->demo_account : '') }}">
                @if($errors->has('demo_account'))
                    <em class="invalid-feedback">
                        {{ $errors->first('demo_account') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('demo_password') ? 'has-error' : '' }}">
                <label for="demo_password">demo_password</label>
                <input type="text" id="demo_password" name="demo_password" class="form-control" value="{{ old('demo_password', isset($projects) ? $projects->demo_password : '') }}">
                @if($errors->has('demo_password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('demo_password') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('admin_account') ? 'has-error' : '' }}">
                <label for="admin_account">admin_account</label>
                <input type="text" id="admin_account" name="admin_account" class="form-control" value="{{ old('admin_account', isset($projects) ? $projects->admin_account : '') }}">
                @if($errors->has('admin_account'))
                    <em class="invalid-feedback">
                        {{ $errors->first('admin_account') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('admin_password') ? 'has-error' : '' }}">
                <label for="admin_password">admin_password</label>
                <input type="text" id="admin_password" name="admin_password" class="form-control" value="{{ old('admin_password', isset($projects) ? $projects->admin_password : '') }}">
                @if($errors->has('admin_password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('admin_password') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                <label for="link">link</label>
                <input type="link" id="link" name="link" class="form-control" value="{{ old('link', isset($projects) ? $projects->link : '') }}">
                @if($errors->has('link'))
                    <em class="invalid-feedback">
                        {{ $errors->first('link') }}
                    </em>
                @endif
                <p class="helper-block">
          
                </p>
            </div>
            <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                <label for="price">price</label>
                <input type="price" id="price" name="price" class="form-control" value="{{ old('price', isset($projects) ? $projects->price : '') }}">
                @if($errors->has('price'))
                    <em class="invalid-feedback">
                        {{ $errors->first('price') }}
                    </em>
                @endif
                <p class="helper-block">
          
                </p>
            </div>
            <div class="form-group {{ $errors->has('cat_name') ? 'has-error' : '' }}">
                <label for="cat_name">category</label>
                <input type="cat_name" id="cat_name" name="cat_name" class="form-control" value="{{ old('cat_name', isset($projects) ? $projects->cat_name : '') }}">
                @if($errors->has('cat_name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('cat_name') }}
                    </em>
                @endif
                <p class="helper-block">
          
                </p>
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label for="image">image</label>
                <input type="file" id="image" name="image" class="form-control" value="{{ old('image', isset($projects) ? $projects->avatar : '') }}"src=''>
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </em>
                @endif
                <p class="helper-block">
                    
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection