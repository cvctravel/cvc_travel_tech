@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
       show projects
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        name
                    </th>
                    <td>
                        {{ $projects->item_name }}
                    </td>
                </tr>
                <tr>
                    <th>
                       description
                    </th>
                    <td>
                        {{ $projects->description }}
                    </td>
                </tr>
                <tr>
                    <th>
                       user account
                    </th>
                    <td>
                        {{ $projects->demo_account }}
                    </td>
                </tr>
                <tr>
                    <th>
                       user password
                    </th>
                    <td>
                        {{ $projects->demo_password }}
                    </td>
                </tr>
                <tr>
                    <th>
                       admin account
                    </th>
                    <td>
                        {{ $projects->admin_account }}
                    </td>
                </tr>
                <tr>
                    <th>
                    admin password
                    </th>
                    <td>
                        {{ $projects->admin_password }}
                    </td>
                </tr>
                <tr>
                    <th>
                       admin link
                    </th>
                    <td>
                        {{ $projects->admin_link }}
                    </td>
                </tr>
                <tr>
                    <th>
                       price
                    </th>
                    <td>
                        {{ $projects->price}}
                    </td>
                </tr>
                <tr>
                    <th>
                        link
                    </th>
                    <td>
                        {{ $projects->link }}
                    </td>
                </tr>
                <tr>
                    <th>
                        category
                    </th>
                    <td>
                        {{ $projects->cat_name }}
                    </td>
                </tr>
                <tr>
                <tr>
                <tr>
                    <th>
                        image
                    </th>
                    <td>
                    <?php
                            $image=$projects->avatar;
                            ?>
                            

                               <img src="{{ asset ('storage/'.$image) }}" width="50" alt=" no image here"> 
                    </td>
                    
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection