@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        create project
    </div>

    <div class="card-body">
        <form action="{{ route("admin.projects.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('item_name') ? 'has-error' : '' }}">
                <label for="item_name">project name*</label>
                <input type="text" id="item_name" name="item_name" class="form-control" value="">
                @if($errors->has('item_name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('item_name') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">description</label>
                <textarea id="description" name="description" class="form-control"></textarea>
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('demo_account') ? 'has-error' : '' }}">
                <label for="demo_account">demo_account</label>
                <input type="text" id="demo_account" name="demo_account" class="form-control" value="">
                @if($errors->has('demo_account'))
                    <em class="invalid-feedback">
                        {{ $errors->first('demo_account') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('demo_password') ? 'has-error' : '' }}">
                <label for="demo_password">demo_password</label>
                <input type="text" id="demo_password" name="demo_password" class="form-control" value="">
                @if($errors->has('demo_password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('demo_password') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('admin_account') ? 'has-error' : '' }}">
                <label for="admin_account">admin_account</label>
                <input type="text" id="admin_account" name="admin_account" class="form-control" value="">
                @if($errors->has('admin_account'))
                    <em class="invalid-feedback">
                        {{ $errors->first('admin_account') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('admin_password') ? 'has-error' : '' }}">
                <label for="admin_password">admin_password</label>
                <input type="text" id="admin_password" name="admin_password" class="form-control" value="">
                @if($errors->has('admin_password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('admin_password') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('admin_link') ? 'has-error' : '' }}">
                <label for="admin_link">admin_link</label>
                <input type="text" id="admin_link" name="admin_link" class="form-control" value="">
                @if($errors->has('admin_link'))
                    <em class="invalid-feedback">
                        {{ $errors->first('admin_link') }}
                    </em>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label for="image">image</label>
                <input type="file" id="image" name="image" class="form-control" value=""required>
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </em>
                @endif
                <p class="helper-block">
                
                </p>
                <form action="{{ route("admin.projects.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                <label for="link">project link</label>
                <input type="text" id="link" name="link" class="form-control" value="">
                @if($errors->has('link'))
                    <em class="invalid-feedback">
                        {{ $errors->first('link') }}
                    </em>
                @endif
                <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                <label for="price">price</label>
                <input type="number" id="price" name="price" class="form-control" value="">
                @if($errors->has('price'))
                    <em class="invalid-feedback">
                        {{ $errors->first('price') }}
                    </em>
                @endif
                
            </div>

            <div>
            <div class="form-group {{ $errors->has('cat_id') ? 'has-error' : '' }}">
            <label for="cat_id">project category</label>
             <select name="cat_name">
                 <option  value="ecommerce">ecommerce</option>
                 <option  value="hotels_and_cruises">hotels_and_cruises</option>
                 <option  value="tracking">tracking</option>
                 <option  value="design">design</option>
                 <option  value="accounting">accounting</option>
                 <option  value="marketing">marketing</option>
                 <option  value="mobile_applications">mobile_applications</option>
                 <option  value="education">education</option>
                 <option  value="hospital_and_medical">hospital_and_medical</option>
                 <option  value="travel">travel</option>
             </select>
             <br> <br>
                <input class="btn btn-danger" type="submit" value="save">
            </div>
        </form>
    </div>
</div>

@endsection