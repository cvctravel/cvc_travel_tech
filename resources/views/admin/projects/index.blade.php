@extends('layouts.admin')
@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.projects.create") }}">
                {{ 'add projects'}} 
            </a>
        </div>
    </div>

<div class="card">
    <div class="card-header">
        projects list
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            project name
                        </th>
                      
                        <th>
                            price
                        </th>

                        <th>
                            image
                        </th>
                        <th>
                            demo link
                        </th>
                        <th>
                            category
                        </th>
                        <th>
                            user account
                        </th>
                        <th>
                            user password
                        </th>
                        <th>
                             admin account
                        </th>
                        <th>
                           admin password
                        </th>
                        <th>
                            admin link
                        </th>
                       
                        
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($projects as $project)
                        <tr data-entry-id="{{ $project->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $project->item_name ?? '' }}
                            </td>
                           
                            <td>${{$project->price ?? '' }}
                            </td>
                            <td>
                            <?php
                            $image=$project->avatar;
                            ?>
                            

                               <img src="{{ asset ('storage/'.$image) }}" width="50" alt=" no image here"> 
                              
                            </td>
                            
                            <td>
                                {{ $project->link ?? '' }}
                            </td>
                            <td>
                                {{ $project->cat_name ?? '' }}
                            </td>
                            <td>
                                {{ $project->demo_account ?? '' }}
                            </td>
                            <td>
                                {{ $project->demo_password ?? '' }}
                            </td>
                            <td>
                                {{ $project->admin_account ?? '' }}
                            </td>
                            <td>
                                {{ $project->admin_password ?? '' }}
                            </td>
                
                            <td>
                                
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.projects.show', $project->id) }}">
                                        view
                                    </a>
                                
                                
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.projects.edit', $project->id) }}">
                                        edit
                                    </a>
                               
                                
                                    <form action="{{ route('admin.projects.destroy', $project->id) }}" method="POST" onsubmit="return confirm('are you sure');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="delete">
                                    </form>
                                
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.projects.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('project_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection