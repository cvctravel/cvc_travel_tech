@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.blog.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.blogs.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('global.blog.fields.name') }}*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($blog) ? $blog->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.blog.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">{{ trans('global.blog.fields.description') }}</label>
                <textarea id="description" name="description" class="form-control">{{ old('description', isset($blog) ? $blog->description : '') }}</textarea>
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.blog.fields.description_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('main_img') ? 'has-error' : '' }}">
                <label for="main_img">{{ trans('global.blog.fields.main_img') }}</label>
                <input type="file" id="main_img" name="main_img" class="form-control" value="{{ old('main_img', isset($blog) ? $blog->main_img : '') }}">
                @if($errors->has('main_img'))
                    <em class="invalid-feedback">
                        {{ $errors->first('main_img') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.blog.fields.main_img_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('inner_img') ? 'has-error' : '' }}">
                <label for="inner_img">{{ trans('global.blog.fields.inner_img') }}</label>
                <input type="file" id="inner_img" name="inner_img" class="form-control" value="{{ old('inner_img', isset($blog) ? $blog->inner_img : '') }}">
                @if($errors->has('inner_img'))
                    <em class="invalid-feedback">
                        {{ $errors->first('inner_img') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.blog.fields.inner_img_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection