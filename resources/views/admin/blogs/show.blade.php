@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.blog.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.blog.fields.name') }}
                    </th>
                    <td>
                        {{ $blog->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.blog.fields.description') }}
                    </th>
                    <td>
                        {!! $blog->description !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.blog.fields.main_img') }}
                    </th>
                    <td>
                         {{ $blog->main_img }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.blog.fields.inner_img') }}
                    </th>
                    <td>
                         {{ $blog->inner_img }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection