@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.faq.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.faqs.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                <label for="question">{{ trans('global.faq.fields.question') }}*</label>
                <input type="text" id="question" name="question" class="form-control" value="{{ old('question', isset($faq) ? $faq->question : '') }}">
                @if($errors->has('question'))
                    <em class="invalid-feedback">
                        {{ $errors->first('question') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.faq.fields.question_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('answer') ? 'has-error' : '' }}">
                <label for="answer">{{ trans('global.faq.fields.answer') }}*</label>
                <input type="text" id="answer" name="answer" class="form-control" value="{{ old('answer', isset($faq) ? $faq->answer : '') }}">
                @if($errors->has('answer'))
                    <em class="invalid-feedback">
                        {{ $errors->first('answer') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.faq.fields.answer_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
                <label for="category">{{ trans('global.faq.fields.category') }}*</label>
                <input type="text" id="category" name="category" class="form-control" value="{{ old('category', isset($faq) ? $faq->category : '') }}">
                @if($errors->has('category'))
                    <em class="invalid-feedback">
                        {{ $errors->first('category') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.faq.fields.category_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection