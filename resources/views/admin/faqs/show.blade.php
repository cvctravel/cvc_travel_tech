@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.faq.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.faq.fields.question') }}
                    </th>
                    <td>
                        {{ $faq->question }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.faq.fields.answer') }}
                    </th>
                    <td>
                        {!! $faq->answer !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.faq.fields.category') }}
                    </th>
                    <td>
                         {{ $faq->category }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection