@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.about.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.about.fields.name') }}
                    </th>
                    <td>
                        {{ $about->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.about.fields.body') }}
                    </th>
                    <td>
                        {!! $about->body !!}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection