@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.phases.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.phases.fields.name') }}
                    </th>
                    <td>
                        {{ $phase->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.phases.fields.body') }}
                    </th>
                    <td>
                        {!! $phase->body !!}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection