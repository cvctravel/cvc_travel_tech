@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.phases.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.phases.update", [$phase->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('global.phases.fields.name') }}*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($phase) ? $phase->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.phases.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                <label for="body">{{ trans('global.phases.fields.body') }}*</label>
                <input type="text" id="body" name="body" class="form-control" value="{{ old('body', isset($phase) ? $phase->body : '') }}">
                @if($errors->has('body'))
                    <em class="invalid-feedback">
                        {{ $errors->first('body') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.phases.fields.body_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection