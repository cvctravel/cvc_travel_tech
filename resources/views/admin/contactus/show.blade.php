@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.Connectus.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.Connectus.fields.name') }}
                    </th>
                    <td>
                        {{ $contactus->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.Connectus.fields.email') }}
                    </th>
                    <td>
                        {{ $contactus->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.Connectus.fields.phone') }}
                    </th>
                    <td>
                        {{ $contactus->phone }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.Connectus.fields.msg') }}
                    </th>
                    <td>
                        {{ $contactus->massage }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection