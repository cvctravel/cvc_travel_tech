@extends('layouts.min')


@section('content')



			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>Blog Details</h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
							
								<li class="breadcrumb-item active">
									Blog Details
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="ls s-pt-50 s-pb-50 s-pb-lg-100 blog-list">
				<div class="container">
					<div class="d-none d-lg-block divider-65"></div>

					<div class="row c-gutter-60">

						<main class="col-lg-7 col-xl-8 order-lg-1 c-gutter-0">
							<article class="text-md-left vertical-item blog-padding ls ms post type-post status-publish format-standard has-post-thumbnail sticky">
								<div class="row">
									<div class="col-xl-4 col-lg-5 col-md-5 col-xs-12">
										<div class="item-media cover-image">
											<img src="{{ asset ('public/uploads/blogs/'.$blog[0]->main_img) }}" alt="{{$blog[0]->main_img}}">
										</div>
									</div>
									<div class="col-xl-8 col-lg-7 col-md-6 col-xs-12">
										<div class="item-content">
											<h6 class="fw-500">
												<a href="/blog-list/{{$blog[0]->id}}">{{$blog[0]->name}}</a>
											</h6>
											<p>
											{{ substr(strip_tags($blog[0]->description),0,119)}}
											<a href="/blog-list/{{$blog[0]->id}}" class="btn-link">Read More...</a>
											</p>
											<div class="item-meta color-darkgrey">
											
											
												
											</div>
										</div>
									</div>
								</div>
							</article>
							<!-- #post-## -->

							<article class="text-md-left vertical-item blog-padding ls ms post type-post status-publish format-standard has-post-thumbnail sticky">
								<div class="row">
									<div class="col-xl-4 col-lg-5 col-md-5 col-xs-12">
										<div class="item-media cover-image">
											<img src="{{ asset ('public/uploads/blogs/'.$blog[1]->main_img) }}" alt="{{$blog[1]->main_img}}">
										</div>
									</div>
									<div class="col-xl-8 col-lg-7 col-md-6 col-xs-12">
										<div class="item-content">
											<h6 class="fw-500">
												<a href="/blog-list/{{$blog[1]->id}}">{{$blog[1]->name}}</a>
											</h6>
											<p>
											{{ substr(strip_tags($blog[1]->description),0,121)}}
											<a href="/blog-list/{{$blog[1]->id}}" class="btn-link">Read More...</a>
											</p>
											<div class="item-meta color-darkgrey">
												
												
												
											</div>
										</div>
									</div>
								</div>
							</article>
							<!-- #post-## -->

							<article class="text-md-left vertical-item blog-padding ls ms post type-post status-publish format-standard has-post-thumbnail sticky">
								<div class="row">
									<div class="col-xl-4 col-lg-5 col-md-5 col-xs-12">
										<div class="item-media cover-image">
											<img src="{{ asset ('public/uploads/blogs/'.$blog[2]->main_img) }}" alt="{{$blog[2]->main_img}}">
										</div>
									</div>
									<div class="col-xl-8 col-lg-7 col-md-6 col-xs-12">
										<div class="item-content">
											<h6 class="fw-500">
												<a href="/blog-list/{{$blog[2]->id}}">{{$blog[2]->name}}</a>
											</h6>
											<p>
											{{ substr(strip_tags($blog[2]->description),0,117)}}
											<a href="/blog-list/{{$blog[2]->id}}" class="btn-link">Read More...</a>
											</p>
											<div class="item-meta color-darkgrey">
											
											</div>
										</div>
									</div>
								</div>
							</article>
							<!-- #post-## -->

							<article class="text-md-left vertical-item blog-padding ls ms post type-post status-publish format-standard has-post-thumbnail sticky">
								<div class="row">
									<div class="col-xl-4 col-lg-5 col-md-5 col-xs-12">
										<div class="item-media cover-image">
											<img src="{{ asset ('public/uploads/blogs/'.$blog[3]->main_img) }}" alt="{{$blog[3]->main_img}}">
										</div>
									</div>
									<div class="col-xl-8 col-lg-7 col-md-6 col-xs-12">
										<div class="item-content">
											<h6 class="fw-500">
												<a href="/blog-list/{{$blog[3]->id}}">{{$blog[3]->name}}</a>
											</h6>
											<p>
											{{ substr(strip_tags($blog[3]->description),0,126)}}
											<a href="/blog-list/{{$blog[3]->id}}" class="btn-link">Read More...</a>
											</p>
											<div class="item-meta color-darkgrey">
												
											</div>
										</div>
									</div>
								</div>
							</article>
							<!-- #post-## -->

							<article class="text-md-left vertical-item blog-padding ls ms post type-post status-publish format-standard has-post-thumbnail sticky">
								<div class="row">
									<div class="col-xl-4 col-lg-5 col-md-5 col-xs-12">
										<div class="item-media cover-image">
											<img src="{{ asset ('public/uploads/blogs/'.$blog[4]->main_img) }}" alt="{{$blog[4]->main_img}}">
										</div>
									</div>
									<div class="col-xl-8 col-lg-7 col-md-6 col-xs-12">
										<div class="item-content">
											<h6 class="fw-500">
												<a href="/blog-list/{{$blog[4]->id}}">{{$blog[4]->name}}</a>
											</h6>
											<p>
											{{ substr(strip_tags($blog[4]->description),0,120)}}
											<a href="/blog-list/{{$blog[4]->id}}" class="btn-link">Read More...</a>
											</p>
											<div class="item-meta color-darkgrey">
												
												
											</div>
										</div>
									</div>
								</div>
							</article>
							<!-- #post-## -->

							<article class="text-md-left vertical-item blog-padding ls ms post type-post status-publish format-standard has-post-thumbnail sticky">
								<div class="row">
									<div class="col-xl-4 col-lg-5 col-md-5 col-xs-12">
										<div class="item-media cover-image">
											<img src="{{ asset ('public/uploads/blogs/'.$blog[5]->main_img) }}" alt="{{$blog[5]->main_img}}">
										</div>
									</div>
									<div class="col-xl-8 col-lg-7 col-md-6 col-xs-12">
										<div class="item-content">
											<h6 class="fw-500">
												<a href="/blog-list/{{$blog[5]->id}}">{{$blog[5]->name}}</a>
											</h6>
											<p>
										    {{ substr(strip_tags($blog[5]->description),0,115)}}
											<a href="/blog-list/{{$blog[5]->id}}" class="btn-link">Read More...</a>
											</p>
											<div class="item-meta color-darkgrey">
												
											</div>
										</div>
									</div>
								</div>
							</article>
							<!-- #post-## -->
							
						</main>

						<aside class="col-lg-5 col-xl-4 order-lg-2">
					


						

							<div class="widget widget_slider">

								<h3 class="widget-title">Recent Posts</h3>
								<div class="item-media">
									<div class="owl-carousel gallery-owl-nav" data-nav="true" data-loop="false" data-autoplay="true" data-items="1" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-responsive-xs="1">
                                    @foreach($blog as $key=>$blogs)
										<div class="vertical-item">
											<div class="item-media">
												<img src="{{ asset ('public/uploads/blogs/'.$blogs->inner_img) }}" alt="{{$blogs->name}}">
											</div>
											<div class="item-content widget-content ls ms">
												<p class="widget-content"></p>
														{{$blogs->name}}
													</a>
												</p>
												

											</div>
										</div>
									@endforeach	
									</div>
									<!-- .owl-carousel -->
								</div>

							</div>
							<!-- .widget_slider -->


						


							<div class="widget widget_flickr">

							
								<ul class="flickr_ul"></ul>
							</div>

							<div class="widget widget_tag_cloud">


								
							</div>


						</aside>

						<div class="d-none d-lg-block divider-105"></div>
					</div>

				</div>
			</section>
			@endsection