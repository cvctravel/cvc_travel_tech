<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->


<head>
	<title>cvctravel.tech</title>
	<meta charset="utf-8">
	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="{{ asset('Frontend/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('Frontend/css/animations.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('Frontend/css/main.css') }}" class="color-switcher-link">
	<script src="{{ asset('Frontend/js/vendor/modernizr-2.6.2.min.js') }}"></script>

<style>
    .page_footer.bordered-footer .footer_logo img {
   left: 445px;
    margin-top: -157px;
    position: absolute;
    width: 250px;
}
.page_footer.bordered-footer .blog-footer a{
    color:white;
}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161767888-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-161767888-1');
</script>

</head>

<body>

	<div class="preloader">
		<div class="preloader_image"></div>
	</div>
