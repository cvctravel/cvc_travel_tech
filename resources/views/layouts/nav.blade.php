	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="widget widget_search">
			<form method="get" class="searchform search-form" action="http://webdesign-finder.com/">
				<div class="form-group">
					<input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
				</div>
				<button type="submit" class="btn">Search</button>
			</form>
		</div>
	</div>

	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls p-normal">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->

		</div>
	</div>
	<!-- eof .modal -->

	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">

			<!-- template sections -->


			<!-- header with three Bootstrap columns - left for logo, center for navigation and right for includes-->
			<header class="page_header ds" style="height: 85px;">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-xl-2 col-lg-3 col-11">
							<a href="/" class="logo text-center">
								<img src="{{asset('Frontend/img/0.png')}}" alt="">
							</a>
						</div>
						<div class="col-xl-8 col-lg-6 col-1 text-lg-center  d-lg-block"  style="height: 85px;">
							<!-- main nav start -->
							<nav class="top-nav">
							<ul class="nav sf-menu" style="">


									<li class="active">
										<a href="{{ asset('/') }}">Home</a>
									</li>
									
									
									<li>
										<a href="{{url('about')}}">About</a>
									</li>
									
									
									<li>
										<a href="{{url('service')}}">Services</a>
									
									</li>
							

									<li>
										<a href="{{url('projects?cat=all')}}">Projects</a>
									
									</li>


									<li>
										<a href="{{url('blog')}}">Blog</a>
									</li>

									<li>
										<a href="{{url('contact')}}">Contact</a>
									</li>
									
						

								</ul>


							</nav>
							<!-- eof main nav -->
						</div>
						<div class="col-xl-2 col-lg-3 text-lg-left text-xl-right d-none d-lg-block">
							<div class="header_phone">
								<h6>
									<span>+20 12</span>21232504
								</h6>
							</div>
						</div>
						<!-- <div class="search_modal affix-top">
                <a href="#" class="search_modal_button header-button">
                    <i class="fa fa-search"></i>
                </a>
            </div> -->
					</div>
				</div>
				<!-- header toggler -->
				<span class="toggle_menu">
					<span></span>
				</span>
			</header>
