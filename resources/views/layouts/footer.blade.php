




<!--			<section class="about-map ms page_map" data-draggable="false" data-scrollwheel="false">-->

<!--<div class="marker">-->
<!--    <div class="marker-address">2231 Sycamore, Green Bay, WI 54304</div>-->
<!--    <div class="marker-description">-->

<!--        <ul class="list-unstyled">-->
<!--            <li>-->
<!--                <span class="icon-inline">-->
<!--                    <span class="icon-styled color-main">-->
<!--                        <i class="fa fa-map-marker"></i>-->
<!--                    </span>-->

<!--                    <span>-->
<!--                        2231 Sycamore, Green Bay, WI 54304-->
<!--                    </span>-->
<!--                </span>-->
<!--            </li>-->

<!--            <li>-->
<!--                <span class="icon-inline">-->
<!--                    <span class="icon-styled color-main">-->
<!--                        <i class="fa fa-phone"></i>-->
<!--                    </span>-->

<!--                    <span>-->
<!--                     + 2 22 68 24 28-->
<!--                    </span>-->
<!--                </span>-->
<!--            </li>-->
<!--            <li>-->
<!--                <span class="icon-inline">-->
<!--                    <span class="icon-styled color-main">-->
<!--                        <i class="fa fa-envelope"></i>-->
<!--                    </span>-->

<!--                    <span>-->
<!--                    cvctravel.tech-->
<!--                    </span>-->
<!--                </span>-->
<!--            </li>-->
<!--        </ul>-->
<!--    </div>-->

<!--    <img class="marker-icon" src="{{asset('Frontend/images/map_marker_icon.png')}}" alt="">-->
<!--</div>-->
<!-- .marker -->

<!--</section>-->


<footer class="page_footer ds s-pb-100 s-pt-100 c-gutter-60 s-parallax bordered-footer bottom-none" style="background-position: 50% 3px;">
<div class="container">
    <center><img src="{{asset('Frontend/images/techlogo.png')}}" alt="cvctravel"style="width:199px;"class="ccc"></center>
    <div class="row">




        <div class="col-md-4 text-center animate footer-contact animated fadeInUp" data-animation="fadeInUp">
            <div class="widget widget_icons_list">



                <h3 class="widget-title name"style="margin-top:20px">CVC Travel
                </h3>
                <p class="next">
                    We create experiences that change the way people interact with brands – and with each other.
                </p>
                <ul>
                    <li class="icon-inline">
                        <div class="icon-styled icon-top color-main fs-14">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <p>11 Aladeeb  Ali adham , Sheraton, Cairo, Egypt</p>
                    </li>
                    <li class="icon-inline">
                        <div class="icon-styled icon-top color-main fs-14">
                            <i class="fa fa-phone"></i>
                        </div>
                <h6>

                    <span>+20 12</span>21232504
                </h6>
                    </li>
                    <li class="icon-inline">
                        <div class="icon-styled icon-top color-main fs-14">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <p>
                            <a href="#" style="color:white;">info@cvctravel.tech</a>
                        </p>
                    </li>
                </ul>
            <div class="widget widget_social_buttons">
                    <a href="https://www.facebook.com/Cadence-Vacations-Tech-Center-101070314991103/" target="blanck" class="fa fa-facebook color-bg-icon rounded-icon" style="background-color:#3b5ca0ك title="Facebook"></a>
                    <a href="https://www.youtube.com/channel/UC4Haixf-eV75G4PnGzt2duQ?view_as=subscriber" target="blanck" class="fa fa-youtube color-bg-icon rounded-icon " title="Youtube"></a>
                    <a href="https://wa.me/201221232504" target="blanck" class="fa fa-whatsapp color-bg-icon rounded-icon" style="background-color:#009688" title="Whatsapp"></a>
                    <!--<a href="#" class="fa fa-linkdin color-bg-icon rounded-icon" title="lindkin">-->
                        <!--<img src="Frontend/images/17893955031555589921-256.png" style="width: 45px;">-->

                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center animate mailchimp animated fadeInUp" data-animation="fadeInUp" style="margin-top: 5px;">
            <div class="widget widget_mailchimp">

                <h3 class="widget-title"style="margin-top:20px">About Us</h3>

                <p>
                "We love what we do and we love helping other people succeed at what they love to do."
                </p>



            </div>
        </div>
        <div class="col-md-4 text-center animate animated fadeInUp" data-animation="fadeInUp">

            <div class="widget widget_text">
                <div class="blog-footer">
                    <h3 class="widget-title next-image-background"style="margin-top:20px">Recent Post</h3>
                    <ul style="list-style-type:none;">
                    <li>
                       <a href="{{url('/')}}">Home</a>
                    </li>
                    <li>
                       <a href="{{url('about')}}">About</a>
                    </li>
                    <li>
                        <a href="{{url('service')}}">Services</a>
                    </li>

                    <li>
                        <a href="{{url('projects?cat=all')}}">Projects</a>
                    </li>


                    <li>
                        <a href="{{url('blog')}}">Blog</a>
                    </li>

                    </ul>


                </div>
            </div>
        </div>


        <div class="divider-10 d-none d-xl-block"></div>
    </div>
</div>
</footer>


</div>
<!-- eof #box_wrapper -->
</div>
<!-- eof #canvas -->


<script src="{{ asset('Frontend/js/compressed.js') }}"></script>
<script src="{{ asset('Frontend/js/main.js') }}"></script>


<!-- Google Map Script -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

</body>

<style>.s-pt-100>[class*=container] {

padding-top: 50px

}


</style>

</html>
