@extends('layouts.min')


@section('content')
<style>
    .page_footer.bordered-footer .footer_logo img {
   left: 445px;
    margin-top: -157px;
    position: absolute;
    width: 250px;
}
.page_footer.bordered-footer .blog-footer a{
    color:white;
}
</style>
</head>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

	<div class="preloader">
		<div class="preloader_image"></div>
	</div>

	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="widget widget_search">
			<form method="get" class="searchform search-form" action="http://webdesign-finder.com/">
				<div class="form-group">
					<input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
				</div>
				<button type="submit" class="btn">Search</button>
			</form>
		</div>
	</div>

	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls p-normal">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->

		</div>
	</div>
	<!-- eof .modal -->

	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">

			<!-- template sections -->


			<!-- header with three Bootstrap columns - left for logo, center for navigation and right for includes-->
		


			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>About</h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index-2.html">Home</a>
								</li>
								<!--<li class="breadcrumb-item">-->
								<!--	<a href="#">Pages</a>-->
								<!--</li>-->
								<li class="breadcrumb-item active">
									About
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="s-pt-30 s-pt-lg-50 ls about">
				<div class="divider-60 d-none d-xl-block"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<div class="main-content text-center">
							   
								<h5>
									{!! $about[0]->body !!}
								</h5>
							
								<i class="rt-icon2-user"></i>
								<p>
								
									<span class="link-a">
										<a href="#"></a>
									</span>
								</p>

								<div class="divider-10 d-none d-xl-block"></div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="s-pt-0  s-pb-30 s-pt-lg-30 s-pb-lg-75 ls about-icon teaser-contact-icon">
				<div class="divider-10 d-none d-xl-block"></div>
				<div class="container">
					<div class="row c-mt-50 c-mt-lg-0">
						<div class="col-lg-4 text-center call-icon">
							<div class="border-icon">
								<div class="teaser-icon">
									<img src="{{asset('Frontend/images/icon1_about.png')}}" alt="">
								</div>
							</div>	
							<div class="icon-content">
							<h6>
							     {{$about[1]->name}}
							</h6>
						     <p>
								 {!! $about[1]->body !!}
							</p>
							</div>
						</div>
						<div class="col-lg-4 text-center write-icon">
							<div class="divider-30 d-none d-xl-block"></div>
							<div class="border-icon">
								<div class="teaser-icon">
								    
									<img src="{{asset('Frontend/images/icon2_about.png')}}" alt="">
								</div>
							</div>
							<div class="icon-content">
								<h6>
									{{$about[2]->name}}
								</h6>
								<p>
									{!! $about[2]->body !!}
								</p>
							</div>
						</div>
						<div class="col-lg-4 text-center visit-icon">
							<div class="border-icon">
								<div class="teaser-icon">
									<img src="{{asset('Frontend/images/icon3_about.png')}}" alt="">
								</div>
							</div>
							<div class="icon-content">
								<h6>
									{{$about[3]->name}}
								</h6>
								<p>
									{!! $about[3]->body !!}
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="s-pt-20 s-pt-lg-30 gallery-carousel main-gallery container-px-0">
				<div class="container-fluid">
					<div class="divider-5 d-none d-xl-block"></div>
					<div class="row">
						<div class="col-lg-12">
							<!-- .owl-carousel-->
						</div>
					</div>
				</div>
			</section>

		

		


					@endsection
	<!-- eof #canvas -->


			