@extends('layouts.min')


@section('content')

			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>Blog Details</h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="/">Home</a>
								</li>
							
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="ls s-pt-30 s-pb-50 s-pt-lg-50 s-pb-lg-100 c-gutter-60 post2">
				<div class="container">
					<div class="row">

						<div class="d-none d-lg-block divider-65"></div>

						<main class="col-lg-7 col-xl-8">
							<article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

								<!-- .post-thumbnail -->


								<div class="post-thumb ds">
									<img src="{{asset('public/uploads/blogs/'.$post->inner_img)}}" alt="">
								
								</div>


								<div class="item-content hero-bg blog-post blog-post1">
									<div class="entry-content">
									    <div>
											<blockquote class="post-block">
											    <h6>
													{{$post->name}}
												</h6>

												<p class="small-text color-main">
													Paradox inc
												</p>
												<h6>Josephine B. Anderson</h6>
												<div class="item-media cover-image">
										        </div>
											</blockquote>
										</div>
										
										<p>
										   {{$post->description}}
										</p>
										
									</div>
								</div>
							</article>

				

							<div class="d-none d-lg-block divider-60"></div>

							<nav class="navigation post-nav" role="navigation">
								<h2 class="screen-reader-text">Post navigation</h2>
							
							</nav>
						</main>


		

			
							<!-- .widget_slider -->


						
						</aside>

						<div class="d-none d-lg-block divider-105"></div>
					</div>

				</div>
			</section>

								@endsection


			


	