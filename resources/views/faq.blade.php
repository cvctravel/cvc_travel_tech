@extends('layouts.min')

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
@section('content')
	<style>
    .page_footer.bordered-footer .footer_logo img {
   left: 445px;
    margin-top: -157px;
    position: absolute;
    width: 250px;
}
.page_footer.bordered-footer .blog-footer a{
    color:white;
}
</style>
</head>
<body>


	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

	<div class="preloader">
		<div class="preloader_image"></div>
	</div>

	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="widget widget_search">
			<form method="get" classAsk Any Questions
="searchform search-form" action="http://webdesign-finder.com/">
				<div class="form-group">
					<input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
				</div>
				<button type="submit" class="btn">Search</button>
			</form>
		</div>
	</div>

	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls p-normal">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->

		</div>
	</div>
	<!-- eof .modal -->

	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">

			<!-- template sections -->


			<!-- header with three Bootstrap columns - left for logo, center for navigation and right for includes-->
		


			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>FAQ</h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								
								<li class="breadcrumb-item active">
									FAQ 
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="s-pt-30 s-pb-20 s-pt-md-75 s-pb-md-50 faq2">
				<div class="divider-35 d-none d-xl-block"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<!-- tabs start -->
							<ul class="nav nav-tabs" role="tablist">
								<li class="nav-item">
									<a class="nav-link" id="tab03" data-toggle="tab" href="#tab03_pane" role="tab" aria-controls="tab03_pane">System Solution</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="tab04" data-toggle="tab" href="#tab04_pane" role="tab" aria-controls="tab04_pane">Technical Support</a>
								</li>

							</ul>
							<div class="tab-content ls ms">
                              
                              <div class="tab-pane fade" id="tab03_pane" role="tabpanel" aria-labelledby="tab03">
									<div id="accordion03" role="tablist">
										<div class="tab-block">
											<div role="tab" id="collapse20_header">
												<p class="text-normal">
													<a data-toggle="collapse" href="#collapse20" aria-expanded="true" aria-controls="collapse20">
														{{$faqs[1]->question ?? ''}}
													</a>
												</p>
											</div>

											<div id="collapse20" class="collapse show" role="tabpanel" aria-labelledby="collapse20_header" data-parent="#accordion03">
												<div class="card-body">
													<p>
														{{$faqs[1]->answer ?? ''}}
													</p>

												</div>
											</div>
										</div>

										<div class="tab-block">
											<div role="tab" id="collapse21_header">
												<p class="text-normal">
													<a class="collapsed" data-toggle="collapse" href="#collapse21" aria-expanded="false" aria-controls="collapse21">
														{{$faqs[2]->question ?? ''}}
													</a>
												</p>
											</div>
											<div id="collapse21" class="collapse" role="tabpanel" aria-labelledby="collapse21_header" data-parent="#accordion03">
												<div class="card-body">
													<p>
														{{$faqs[2]->answer  ?? '' }}
													</p>
												</div>
											</div>
										</div>

										<div class="tab-block">
											<div role="tab" id="collapse22_header">
												<p class="text-normal">
													<a class="collapsed" data-toggle="collapse" href="#collapse22" aria-expanded="false" aria-controls="collapse22">
														{{$faqs[3]->question ?? '' }}
													</a>
												</p>
											</div>
											<div id="collapse22" class="collapse" role="tabpanel" aria-labelledby="collapse22_header" data-parent="#accordion03">
												<div class="card-body">
													<p>
														{{$faqs[3]->answer ?? '' }}
													</p>
												</div>
											</div>
										</div>

										<div class="tab-block">
											<div role="tab" id="collapse23_header">
												<p class="text-normal">
													<a class="collapsed" data-toggle="collapse" href="#collapse23" aria-expanded="false" aria-controls="collapse23">
														{{$faqs[4]->question ?? '' }}
													</a>
												</p>
											</div>
											<div id="collapse23" class="collapse" role="tabpanel" aria-labelledby="collapse23_header" data-parent="#accordion03">
												<div class="card-body">
													<p>
														{{$faqs[4]->answer ?? '' }}
													</p>
												</div>
											</div>
										</div>

										<div class="tab-block">
											<div role="tab" id="collapse24_header">
												<p class="text-normal">
													<a class="collapsed" data-toggle="collapse" href="#collapse24" aria-expanded="false" aria-controls="collapse24">
														{{$faqs[5]->question ?? '' }}
													</a>
												</p>
											</div>
											<div id="collapse24" class="collapse" role="tabpanel" aria-labelledby="collapse24_header" data-parent="#accordion03">
												<div class="card-body">
													<p>
														{{$faqs[5]->answer ?? '' }}
													</p>
												</div>
											</div>
										</div>
									</div>
								<!-- collapse -->
							   </div>

								<div class="tab-pane fade" id="tab04_pane" role="tabpanel" aria-labelledby="tab04">
									<div id="accordion04" role="tablist">
										<div class="tab-block">
											<div role="tab" id="collapse28_header">
												<p class="text-normal">
													<a data-toggle="collapse" href="#collapse28" aria-expanded="true" aria-controls="collapse28">
														{{$faqs[6]->question ?? '' }}
													</a>
												</p>
											</div>

											<div id="collapse28" class="collapse show" role="tabpanel" aria-labelledby="collapse28_header" data-parent="#accordion04">
												<div class="card-body">
													<p>
														{{$faqs[6]->answer ?? ''}}
													</p>

												</div>
											</div>
										</div>

										<div class="tab-block">
											<div role="tab" id="collapse29_header">
												<p class="text-normal">
													<a class="collapsed" data-toggle="collapse" href="#collapse29" aria-expanded="false" aria-controls="collapse29">
														{{$faqs[7]->question ?? '' }}
													</a>
												</p>
											</div>
											<div id="collapse29" class="collapse" role="tabpanel" aria-labelledby="collapse29_header" data-parent="#accordion04">
												<div class="card-body">
													<p>
														L{{$faqs[7]->answer ?? ''}}
													</p>
												</div>
											</div>
										</div>

										<div class="tab-block">
											<div role="tab" id="collapse30_header">
												<p class="text-normal">
													<a class="collapsed" data-toggle="collapse" href="#collapse30" aria-expanded="false" aria-controls="collapse30">
														{{$faqs[8]->question ?? ''}}
													</a>
												</p>
											</div>
											<div id="collapse30" class="collapse" role="tabpanel" aria-labelledby="collapse30_header" data-parent="#accordion04">
												<div class="card-body">
													<p>
														{{$faqs[8]->answer ?? ''}}
													</p>
												</div>
											</div>
										</div>

										<div class="tab-block">
											<div role="tab" id="collapse31_header">
												<p class="text-normal">
													<a class="collapsed" data-toggle="collapse" href="#collapse31" aria-expanded="false" aria-controls="collapse31">
														{{$faqs[9]->question ?? '' }}
													</a>
												</p>
											</div>
											<div id="collapse31" class="collapse" role="tabpanel" aria-labelledby="collapse31_header" data-parent="#accordion04">
												<div class="card-body">
													<p>
														{{$faqs[9]->answer ?? '' }}
													</p>
												</div>
											</div>
										</div>

									</div>
									<!-- collapse -->
								</div>
							</div>
							<!-- tabs end-->
						</div>
					</div>
				</div>
			</section>

			<section class="s-pb-75 s-pb-lg-100 faq-form">
				<div class="divider-20 d-none d-xl-block"></div>
				<div class="container">
					<div class="form-header text-center">
						<h4>
							Ask Any Questions
						</h4>
					</div>
					<form class="c-mb-20 c-gutter-20" action="{{route('fs')}}" method="POST">
					{{ csrf_field() }}
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group has-placeholder">
									<label for="name">Full Name
										<span class="required">*</span>
									</label>
									<input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group has-placeholder">
									<label for="email">Email address
										<span class="required">*</span>
									</label>
									<input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email Adress">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group has-placeholder">
									<label for="phone">Phone Number
										<span class="required">*</span>
									</label>
									<input type="text" aria-required="true" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Phone Number">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group has-placeholder">
									<label for="message">Your Message</label>
									<textarea aria-required="true" rows="6" cols="45" name="message" id="message" class="form-control" placeholder="Your Message"></textarea>
								</div>
							</div>
						</div>
						<div class="row c-mt-0 c-mt-lg-15">
							<div class="col-sm-12 text-center ">
								<div class="form-group">
									<button type="submit" id="contact_form_submit" name="contact_submit"  class="btn btn-maincolor">Ask a questions
									</button>
								</div>
							</div>

						</div>
					</form>
				</div>
				<div class="divider-20 d-none d-xl-block"></div>
			</section>

			<section class="s-py-100 ls faq-contact-icon">
				<div class="divider-20 d-none d-xl-block"></div>
				<div class="container">
					<div class="row c-mb-50 c-gutter-30">
						<div class="col-lg-4 text-center">
							<div class="box-icon">
								<div class="border-icon">
									<div class="teaser-icon">
										<img src="{{asset ('public/uploads/settings/'.$setting[8]->img)}}" alt="">
									</div>
								</div>
								<h6>
									{!!$setting[8]->name!!}
								</h6>
								<p>
								
									<!--<strong>Support:</strong>--> {!!$setting[8]->body!!}
								</p>
							</div>
						</div>
						<div class="col-lg-4 text-center">
							<div class="box-icon">
								<div class="border-icon">
									<div class="teaser-icon">
										<img src="{{asset('public/uploads/settings/'.$setting[9]->img)}}" alt="" width="77px;">
									</div>
								</div>
								<h6>
									{!!$setting[9]->name!!}
								</h6>
								<p>
									{!!$setting[9]->body!!}
									
								</p>
							</div>
						</div>
						<div class="col-lg-4 text-center">
							<div class="box-icon">
								<div class="border-icon">
									<div class="teaser-icon">
										<img src="{{asset('public/uploads/settings/'.$setting[17]->img)}}" alt="">
									</div>
								</div>
								<h6>
									{!!$setting[17]->name!!}
								</h6>
								<p>
									{!!$setting[17]->body!!}
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="divider-30 d-none d-xl-block"></div>
			</section>


@endsection

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> 
<script>
	@if(Session::has('message'))
		var type="{{Session::get('alert-type','info')}}"
        switch(type){
	        case 'success':
	            toastr.success("{{ Session::get('message') }}");
	            break;
         	 case 'error':
		        toastr.error("{{ Session::get('message') }}");
		        break;
                    }
	@endif
</script>

<!-- Mirrored from webdesign-finder.com/html/dotcreative/faq-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 21 Mar 2020 20:45:05 GMT -->
</html>
